package dtu.ws.opuranus.interfaces;

import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.*;

import java.math.BigDecimal;

/**
 * @author Mathias
 */
public interface PaymentGatewayInterface {

    /**
     * @param merchant
     * @param tokenId
     * @param amount
     * @param description
     * @return
     * @throws CustomerException
     * @throws MerchantException
     * @throws TokenException
     * @throws BankException
     */
    public int doTransaction(Merchant merchant, Integer tokenId, BigDecimal amount, String description) throws CustomerException, MerchantException, TokenException, BankException;

    /**
     * @param transactionId
     * @param description
     * @return
     * @throws BankException
     * @throws TransactionException
     * @throws MerchantException
     * @throws CustomerException
     */
    public String doRefund(int transactionId, String description) throws BankException, TransactionException, MerchantException, CustomerException;
}

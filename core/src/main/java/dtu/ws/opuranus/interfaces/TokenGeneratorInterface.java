package dtu.ws.opuranus.interfaces;

import dtu.ws.opuranus.entity.Token;

/**
 * @author Nicklas
 */
public interface TokenGeneratorInterface {
    public Token generateToken(int identity);
}

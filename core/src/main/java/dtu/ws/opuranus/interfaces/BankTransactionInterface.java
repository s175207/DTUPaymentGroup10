package dtu.ws.opuranus.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Heino
 */
public interface BankTransactionInterface {

//    public BankTransactionInterface(Date time, BigDecimal amount, BigDecimal balance, String creditor, String debtor, String description);

    /**
     * @return
     */
    @Override
    public String toString();

    /**
     * @return
     */
    public Date getTime();

    /**
     * @return
     */
    public BigDecimal getAmount();

    /**
     * @return
     */
    public BigDecimal getBalance();

    /**
     * @return
     */
    public String getCreditor();

    /**
     * @return
     */
    public String getDebtor();

    /**
     * @return
     */
    public String getDescription();
}

package dtu.ws.opuranus.interfaces;

import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.BankException;
import dtu.ws.opuranus.exceptions.TokenException;

import java.util.List;
import java.util.Set;

/**
 * @author Anders
 */
public interface TokenManagerInterface {

    /**
     * @param customerId
     * @return
     */
    public Token generateAndSaveToken(int customerId);

    /**
     * @param tokenId
     * @return
     * @throws TokenException
     */
    public Token getTokenById(Integer tokenId) throws TokenException;

    /**
     * @param customerId
     * @return
     * @throws TokenException
     */
    public List<Token> requestTokens(int customerId) throws TokenException;

    /**
     * @param customer
     * @return
     * @throws TokenException
     */
    public List<Token> requestTokens(Customer customer) throws TokenException;

    /**
     * @param customerId
     * @param max
     * @return
     * @throws TokenException
     */
    public List<Token> requestTokens(int customerId, int max) throws TokenException, BankException;

    /**
     * @param customer
     * @param max
     * @return
     * @throws TokenException
     */
    public List<Token> requestTokens(Customer customer, int max) throws TokenException, BankException;

    /**
     * @param token
     * @throws TokenException
     */
    public void useToken(Token token) throws TokenException;

    /**
     * @param customerId
     * @return
     */
    public int getNumberOfTokens(Integer customerId);

    /**
     * @param customer
     * @return
     */
    public int getNumberOfTokens(Customer customer);

    /**
     * @param customerId
     * @return
     */
    public List<Token> getTokens(Integer customerId);

    /**
     * @param customer
     * @return
     */
    public List<Token> getTokens(Customer customer);

    /**
     * @param customer
     * @return
     */
    public List<Token> getTokens(Merchant customer);

    /**
     * @param token
     */
    public void deleteToken(Token token);
}

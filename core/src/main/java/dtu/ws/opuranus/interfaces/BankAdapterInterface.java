package dtu.ws.opuranus.interfaces;

import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.BankException;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Heino
 * BankAdapter establishes a
 */
public interface BankAdapterInterface {


    /**
     *
     * @param fromAccountNumber The account from which to transfer money
     * @param toAccountNumber The account the money are to be transferred to
     * @param amount The amount of money to transfer
     * @param description Information regarding the payment
     * @throws BankException throws BankException
     */
    public void transfer(String fromAccountNumber, String toAccountNumber, BigDecimal amount, String description) throws BankException;

    /**
     *
     * @param firstName of account holder
     * @param lastName of account holder
     * @param id of account
     * @param balance of account
     * @return Returns String
     * @throws BankException Throws BankExcepticon
     */
    public String createAccount(String firstName, String lastName, String id, BigDecimal balance) throws BankException;

    /**
     *
     * @param customer Customer object
     * @param amount BigDecimal amount
     * @return Returns string
     * @throws BankException Throws BankException
     */
    public String createAccount(Customer customer, BigDecimal amount) throws BankException;

    /**
     * Create account
     * @param merchant Merchant object
     * @param amount BigDecimal number
     * @return String
     */
    public String createAccount(Merchant merchant, BigDecimal amount) throws BankException;

    /**
     * Delete account
     * @param accountNumber String accountnumber
     * @throws BankException Throws BankException
     *
     */
    public void deleteAccount(String accountNumber) throws BankException;

    /**
     *Get balanc from account
     * @param accountNumber String accountnumber
     * @return Returns BigDecimal amount
     * @throws BankException throws BankException
     */
    public BigDecimal getAccountBalance(String accountNumber) throws BankException;

    /**
     *Get Account number
     * @param cpr String CPR
     * @return String
     * @throws BankException throw BankException
     */
//  public String getAccountNumberByCprNumber(String cpr) throws BankException;

    /**
     *Get transactions
     * @param accountNumber String accountnumber
     * @return List of BankTransactionInterface
     * @throws BankException throws BankException
     */
    public List<BankTransactionInterface> getAccountTransactions(String accountNumber) throws BankException;

    /**
     *Get name of account's owner
     * @param accountNumber String account number
     * @return String Name
     * @throws BankException throws BankException
     */
//  public String  getAccountOwnerName(String accountNumber) throws BankException;
}

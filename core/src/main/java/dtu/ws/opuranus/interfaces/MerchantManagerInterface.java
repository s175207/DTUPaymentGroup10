package dtu.ws.opuranus.interfaces;

import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.BankException;
import dtu.ws.opuranus.exceptions.CustomerException;
import dtu.ws.opuranus.exceptions.MerchantException;

/**
 * @author Nicklas
 */
public interface MerchantManagerInterface {

    /**
     * @param name
     * @param cpr
     * @return
     * @throws BankException
     */
    public Merchant createMerchant(String name, String cpr) throws BankException;

    /**
     * @param customer
     */
    public void deleteMerchant(Merchant customer);

    /**
     * @return
     */
    public Iterable<Merchant> getMerchantList();

    /**
     * @param name
     * @return
     * @throws MerchantException
     */
    public boolean hasMerchantByName(String name) throws MerchantException;

    /**
     * @param cvr
     * @return
     * @throws MerchantException
     */
    public boolean hasMerchantByCvr(String cvr) throws MerchantException;

    /**
     * @param identity
     * @return
     * @throws MerchantException
     */
    public Merchant getMerchantByIdentity(Integer identity) throws MerchantException;

    /**
     * @param merchant
     * @return
     * @throws MerchantException
     */
    public Merchant saveMerchant(Merchant merchant) throws MerchantException;

    /**
     * @param name
     * @return
     * @throws MerchantException
     */
    public Merchant getMerchantByName(String name) throws MerchantException;

    /**
     * @param cvr
     * @return
     * @throws MerchantException throws MerchantException
     */
    public Merchant getMerchantByCvr(String cvr) throws MerchantException;
}

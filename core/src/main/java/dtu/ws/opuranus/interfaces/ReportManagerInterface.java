package dtu.ws.opuranus.interfaces;


import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.entity.Transaction;
import dtu.ws.opuranus.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Nicklas
 */
public interface ReportManagerInterface {

    /**
     * Gets a string containing the data from the customers transactions for the last 30 days.
     * @param customer
     * @return
     */
    public String transactionReport(Customer customer) throws TransactionException;

    /**
     * Gets a string containing the data from the merchants transactions for the last 30 days.
     * @param merchant
     * @return
     */
    public String transactionReport(Merchant merchant) throws TransactionException;

    /**
     * Log a new transaction in the database.
     * @param customer
     * @param merchant
     * @param token
     * @param amount
     * @return
     */
    public Transaction logNewTransaction(Customer customer, Merchant merchant, Token token, BigDecimal amount);

    /**
     * get a transaction from a transaction id
     * @param transactionId
     * @return
     * @throws TransactionException
     */
    public Transaction getTransactionById(int transactionId) throws TransactionException;

    /**
     * Mark a transaction as refunded.
     * @param transaction
     * @return
     * @throws TransactionException
     */
    public Transaction markRefunded(Transaction transaction) throws TransactionException;

    /**
     * Sets the time of a transaction.
     * @param transaction
     * @param date
     */
    public void setTime(Transaction transaction, Date date);

    /**
     * Deletes all Transactions.
     */
    public void deleteAll();

}

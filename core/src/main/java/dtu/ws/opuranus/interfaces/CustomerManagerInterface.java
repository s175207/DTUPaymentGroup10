package dtu.ws.opuranus.interfaces;

import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.exceptions.CustomerException;

/**
 * @author Nicklas
 */
public interface CustomerManagerInterface {

    /**
     * @param name
     * @param cpr
     * @return
     * @throws CustomerException
     */
    public Customer createCustomer(String name, String cpr) throws CustomerException;

    /**
     * @param customer
     * @throws CustomerException
     */
    public void deleteCustomer(Customer customer) throws CustomerException;

    /**
     * @return
     */
    public Iterable<Customer> getCustomerList();

    /**
     * @param name
     * @return
     */
    public boolean hasCustomerByName(String name);

    /**
     * @param cpr
     * @return
     */
    public boolean hasCustomerByCpr(String cpr);

    /**
     * @param name
     * @return
     * @throws CustomerException
     */
    public Customer getCustomerByName(String name) throws CustomerException;

    /**
     * @param cpr
     * @return
     * @throws CustomerException
     */
    public Customer getCustomerByCpr(String cpr) throws CustomerException;

    /**
     * @param identity
     * @return
     * @throws CustomerException
     */
    public Customer getCustomerByIdentity(Integer identity) throws CustomerException;

    /**
     * @param customer
     * @return
     * @throws CustomerException
     */
    public Customer saveCustomer(Customer customer) throws CustomerException;
}

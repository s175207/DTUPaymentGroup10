package dtu.ws.opuranus;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Nicklas
 */
public class TransferObject
    implements Serializable {

    public String from;
    public String to;
    public BigDecimal amount;
    public String description;
}

package dtu.ws.opuranus;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Heino
 */
public class BankAccountObject
        implements Serializable {

    public String firstName;
    public String lastName;
    public String id;
    public BigDecimal balance;
}

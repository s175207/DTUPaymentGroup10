package dtu.ws.opuranus;

import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Mathias
 */
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class PaymentObject implements Serializable {

    public Merchant from;
    public int tokenId;
    public BigDecimal amount;
    public String description;
}

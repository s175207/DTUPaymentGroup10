package dtu.ws.opuranus;

import dtu.ws.opuranus.entity.Transaction;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

/**
 * @author Nicklas
 */
@Getter
@AllArgsConstructor
public class TransactionTimeTransferObject {

    Transaction transaction;
    Date date;

}

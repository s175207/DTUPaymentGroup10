package dtu.ws.opuranus;

/**
 * @author Nicklas
 */
public class Environment {

    static public String getHostname() {
        System.out.println("Using host: ------");
//        return "fastmoney-10.compute.dtu.dk";
        return getHostname("fastmoney-10.compute.dtu.dk");
    }

    static public String getHostname(String hostname) {

        try {
            String string = System.getenv("hostname");
            if (string.length() > 0) {
                System.out.println("Using host:" + string);
                return string;
            }
        } catch (Exception e) {
        }

        System.out.println("Using host:" + hostname);
        return hostname;
    }
}

package dtu.ws.opuranus;

import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * @author Nicklas
 */
@Getter
public class TransactionTransferObject {


    Customer customer;
    Merchant merchant;
    Token token;
    BigDecimal amount;

    public TransactionTransferObject(Customer customer, Merchant merchant, Token token, BigDecimal amount) {
        this.customer = customer;
        this.merchant = merchant;
        this.token = token;
        this.amount = amount;
    }
}

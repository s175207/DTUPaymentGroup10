package dtu.ws.opuranus.stubs;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtu.ws.opuranus.Environment;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.MerchantException;
import dtu.ws.opuranus.interfaces.MerchantManagerInterface;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/*
 * @Author Nicklas
 */
@Service
public class MerchantManagerStub
        implements MerchantManagerInterface {


    protected ObjectMapper objectMapper;
    private Client client;
    {{
        objectMapper = new ObjectMapper();

        ClientConfig config = new ClientConfig();
        config.register(objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        );

        client = ClientBuilder.newClient();
    }}
    WebTarget webTarget = client.target("http://" + Environment.getHostname() + ":8885/");
    {{
        webTarget.register((ClientRequestFilter) requestContext -> {
            System.out.println(requestContext.getUri());
        });
    }}


    /**
     * Creates a saves a merchant with the given name and cvr
     * @param name Full name of the merchant
     * @param cvr Cvr number of the merchant
     * @return The merchant created
     */
    @Override
    public Merchant createMerchant(String name, String cvr) {
        Merchant merchant = new Merchant();
        merchant.setName(name);
        merchant.setCvr(cvr);

        Response response = webTarget
                .path("merchant")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(
                        merchant,
                        MediaType.APPLICATION_JSON
                ));

        switch (response.getStatus()) {

            case 201:
                return response.readEntity(Merchant.class);

            default:
                throw new RuntimeException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Deletes a merchant with the ID of the merchant object
     * @param merchant The merchant to be deleted
     */
    @Override
    public void deleteMerchant(Merchant merchant) {
        Response response = webTarget
                .path("merchant")
                .path(Integer.toString(merchant.getId()))
                .request()
                .delete();

        if (response.getStatus() == 204) {
            throw new RuntimeException("TODO");
        }
    }

    /**
     * Gets a list of merchants in the database
     * @return Iterable of merchant objects
     */
    @Override
    public Iterable<Merchant> getMerchantList() {
        List<Merchant> merchants = webTarget
                .path("merchant")
                .request()
                .get(new GenericType<List<Merchant>>() {});

        return merchants;
    }

    /**
     * Check to see if a merchant exists with the given name
     * @param name full name of the merchant
     * @return whether or not the merchant exist
     * @throws MerchantException if the server responded with an exception
     */
    @Override
    public boolean hasMerchantByName(String name) throws MerchantException {
        Response response = webTarget
                .path("merchant")
                .path("searchName")
                .queryParam("name", name)
                .request()
                .get();

        switch (response.getStatus()) {

            case 200:
                Merchant merchant = response.readEntity(Merchant.class);
                return merchant != null;

            case 404:
                return false;

            default:
                throw new MerchantException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Check to see if a merchant exists with the given name
     * @param cvr cvr of the merchant
     * @return whether or not the merchant exist
     * @throws MerchantException if the server responded with an exception
     */
    @Override
    public boolean hasMerchantByCvr(String cvr) throws MerchantException {
        Response response = webTarget
                .path("merchant")
                .path("searchCvr")
                .queryParam("cvr", cvr)
                .request()
                .get();

        switch (response.getStatus()) {

            case 200:
                Merchant merchant = response.readEntity(Merchant.class);
                return merchant != null;

            case 404:
                return false;

            default:
                throw new MerchantException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Gets a merchant based on the name identifier
     * @param name name of the merchant
     * @return Merchant object with the given name
     */
    @Override
    public Merchant getMerchantByName(String name) {
        Response response = webTarget
                .path("merchant").path("search")
                .queryParam("name", name)
                .request()
                .get();

        return response.readEntity(Merchant.class);
    }

    /**
     * Gets a merchant based on the cpr identifier
     * @param cpr cpr of the merchant
     * @return Merchant object with the given name
     */
    @Override
    public Merchant getMerchantByCvr(String cpr) {
        Response response = webTarget
                .path("merchant").path("search")
                .queryParam("cpr", cpr)
                .request()
                .get();

        return response.readEntity(Merchant.class);
    }

    /**
     * Gets a merchant based on the identity identifier
     * @param identity name of the merchant
     * @return Merchant object with the given identity
     */
    @Override
    public Merchant getMerchantByIdentity(Integer identity) {
        Response response = webTarget
                .path("merchant").path(identity.toString())
                .request()
                .get();

        return response.readEntity(Merchant.class);
    }

    /**
     * Saves a merchant in the database
     * @param merchant the merchant that should be saved in the database.
     * @return the merchant saved in the database.
     */
    @Override
    public Merchant saveMerchant(Merchant merchant) {
        Response response = webTarget
                .path("merchant").path(merchant.getId().toString())
                .request()
                .put(Entity.entity(
                        merchant,
                        MediaType.APPLICATION_JSON
                ));

        return response.readEntity(Merchant.class);
    }
}

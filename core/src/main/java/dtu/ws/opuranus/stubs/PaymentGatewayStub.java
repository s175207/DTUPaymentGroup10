package dtu.ws.opuranus.stubs;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtu.ws.opuranus.Environment;
import dtu.ws.opuranus.PaymentObject;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.*;
import dtu.ws.opuranus.interfaces.PaymentGatewayInterface;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Service;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/*
 * @Author Mathias
 * PaymentManagerStubs acts as the interface from the PaymentManager to the other micro services of DTUPay
 */
@Service
public class PaymentGatewayStub implements PaymentGatewayInterface {

    private ObjectMapper objectMapper;
    private Client client;
    {{
        objectMapper = new ObjectMapper();

        ClientConfig config = new ClientConfig();
        config.register(objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        );

        client = ClientBuilder.newClient();
    }}
    WebTarget webTarget = client.target("http://" + Environment.getHostname() + ":8882/");
/*
    {{
        webTarget.register((ClientRequestFilter) requestContext -> {
            System.out.println(requestContext.getUri());
        });
    }}
*/

    /**
     * Does a transaction
     * @param merchant Merchant object
     * @param tokenId Integer ID
     * @param amount BigDecimal amount
     * @param description String
     * @return Integer
     * @throws CustomerException throws CustomerException
     * @throws MerchantException throws MerchantException
     * @throws TokenException throws TokenException
     * @throws BankException throws BankException
     */
    @Override
    public int doTransaction(Merchant merchant, Integer tokenId, BigDecimal amount, String description) throws CustomerException, MerchantException, TokenException, BankException {

        PaymentObject paymentObject = new PaymentObject();
        paymentObject.from = merchant;
        paymentObject.tokenId = tokenId;
        paymentObject.amount = amount;
        paymentObject.description = description;

        Response response = webTarget
                .path("payment/")
                .path("transfer/")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(paymentObject, MediaType.APPLICATION_JSON));

        System.out.println("Response HTTP Status Code: " + response.getStatus());
        if(response.getStatus() != 201)
        {
            throw new BankException(response.readEntity(String.class));
        }
        return response.getStatus();
    }

    /**
     * Refund a purchase
     * @param transactionId Integer ID of the transaction
     * @param description Description of the transaction
     * @return Return a string
     * @throws BankException throw BankException
     * @throws TransactionException throw BankException
     * @throws MerchantException throw BankException
     * @throws CustomerException throw BankException
     */
    @Override
    public String doRefund(int transactionId, String description) throws BankException, TransactionException, MerchantException, CustomerException {
        Response response = webTarget
                .path("payment/")
                .path("refund/")
                .queryParam("transactionId" , transactionId)
                .queryParam("description" , description)
                .request(MediaType.APPLICATION_JSON)
                .get();

        if(response.getStatus() == 201){
            return "";
        }
        throw new InternalServerErrorException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }
}
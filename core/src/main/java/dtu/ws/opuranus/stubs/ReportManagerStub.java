package dtu.ws.opuranus.stubs;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtu.ws.opuranus.Environment;
import dtu.ws.opuranus.TransactionTimeTransferObject;
import dtu.ws.opuranus.TransactionTransferObject;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.entity.Transaction;
import dtu.ws.opuranus.exceptions.TransactionException;
import dtu.ws.opuranus.interfaces.ReportManagerInterface;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Service;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Date;

/*
 * @Author Rasmus
 * ReportManagerStubs acts as the interface from the ReportManager to the other micro services of DTUPay
 */
@Service
public class ReportManagerStub implements ReportManagerInterface {

    protected ObjectMapper objectMapper;
    private Client client;
    {{
        objectMapper = new ObjectMapper();

        ClientConfig config = new ClientConfig();
        config.register(objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        );

        client = ClientBuilder.newClient();
    }}
    WebTarget webTarget = client.target("http://" + Environment.getHostname() + ":8883/");
/*
    {{
        webTarget.register((ClientRequestFilter) requestContext -> {
            System.out.println(requestContext.getUri());
        });
    }}
*/

    /**
     * Retrieves a transaction report as a string
     * @param customer Customer object
     * @return Returns a string containing the report
     */
    @Override
    public String transactionReport(Customer customer) {
        Response response = webTarget
                .path("report")
                .path("transactionReportCustomer")
                .queryParam("customerId" , customer.getId())
                .request(MediaType.APPLICATION_JSON)
                .get();

        if (response.getStatus() == 204) {
            return response.readEntity(String.class);
        }
        throw new InternalServerErrorException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }

    /**
     * Retrieves a transaction report as a string
     * @param merchant Merchant object
     * @return Returns a string containing the report
     */
    @Override
    public String transactionReport(Merchant merchant) {
        Response response = webTarget
                .path("report")
                .path("transactionReportMerchant")
                .queryParam("merchantId" , merchant.getId())
                .request(MediaType.APPLICATION_JSON)
                .get();

        if (response.getStatus() == 204) {
            return response.readEntity(String.class);
        }
        throw new InternalServerErrorException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }

    /**
     * Logs transactions
     * @param customer Customer object
     * @param merchant Merchant object
     * @param token Token object
     * @param amount BigDecimal number
     * @return Returns a transaction
     */
    @Override
    public Transaction logNewTransaction(Customer customer, Merchant merchant, Token token, BigDecimal amount) {

        TransactionTransferObject transfer =
                new TransactionTransferObject(customer,merchant,token,amount);

        Response response = webTarget
                .path("report")
                .path("logNewTransaction")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(
                        transfer,
                        MediaType.APPLICATION_JSON)
                        );

        if (response.getStatus() == 204) {
            return response.readEntity(Transaction.class);
        }
        throw new InternalServerErrorException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }

    /**
     * Gets a transaction by it's ID
     * @param transactionId Integer ID
     * @return Transaction object
     * @throws TransactionException Throws TransactionException
     */
    @Override
    public Transaction getTransactionById(int transactionId) throws TransactionException {
        Response response = webTarget
                .path("report")
                .path("getTransactionById")
                .queryParam("transactionId" , transactionId)
                .request(MediaType.APPLICATION_JSON)
                .get();

        if (response.getStatus() == 204) {
            return response.readEntity(Transaction.class);
        }
        throw new InternalServerErrorException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }

    /**
     * Marks a transaction as refunded
     * @param transaction Transaction object
     * @return Transaction object
     * @throws TransactionException Throws TransactionException
     */
    @Override
    public Transaction markRefunded(Transaction transaction) throws TransactionException {
        Response response = webTarget
                .path("report")
                .path("markRefunded")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.entity(
                        transaction,
                        MediaType.APPLICATION_JSON));

        if (response.getStatus() == 204) {
            return response.readEntity(Transaction.class);
        }
        throw new InternalServerErrorException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }

    /**
     * Set time of transaction
     * @param transaction Transaction object
     * @param date Date object
     */
    @Override
    public void setTime(Transaction transaction, Date date) {

        TransactionTimeTransferObject transfer =
                new TransactionTimeTransferObject(transaction, date);

        Response response = webTarget
                .path("report/")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.entity(
                        transfer,
                        MediaType.APPLICATION_JSON));

        if (response.getStatus() != 204) {
            throw new InternalServerErrorException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Delete all records
     */
    @Override
    public void deleteAll() {

        Response response = webTarget
                .path("report")
                .request(MediaType.APPLICATION_JSON)
                .delete();

        if (response.getStatus() != 204) {
            throw new InternalServerErrorException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

}

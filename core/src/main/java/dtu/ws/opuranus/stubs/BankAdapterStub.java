package dtu.ws.opuranus.stubs;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtu.ws.opuranus.BankAccountObject;
import dtu.ws.opuranus.Environment;
import dtu.ws.opuranus.TransferObject;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.BankException;
import dtu.ws.opuranus.interfaces.BankAdapterInterface;
import dtu.ws.opuranus.interfaces.BankTransactionInterface;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

 /**
 * @author Heino
 */
@Service
public class BankAdapterStub
        implements BankAdapterInterface {

    protected ObjectMapper objectMapper;
    private Client client;
    {{
        objectMapper = new ObjectMapper();

        ClientConfig config = new ClientConfig();
        config.register(objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        );

        client = ClientBuilder.newClient();
    }}

    WebTarget webTarget = client.target("http://" + Environment.getHostname() + ":8881/");
    {{
        webTarget.register((ClientRequestFilter) requestContext -> {
            System.out.println(requestContext.getMethod() + " : " + requestContext.getUri());
        });
    }}

    /**
     * Creates a new transfer from a customer to a merchant with an amount and description.
     * @param fromAccountNumber The account from which to transfer money
     * @param toAccountNumber The account the money are to be transferred to
     * @param amount The amount of money to transfer
     * @param description Information regarding the payment
     * @throws BankException
     */
    @Override
    public void transfer(String fromAccountNumber, String toAccountNumber, BigDecimal amount, String description) throws BankException {

        TransferObject transferObject = new TransferObject();
        transferObject.from = fromAccountNumber;
        transferObject.to = toAccountNumber;
        transferObject.amount = amount;
        transferObject.description = description;

        Response response = webTarget
                .path("bank").path("transfer")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(
                        transferObject,
                        MediaType.APPLICATION_JSON
                ));

        switch (response.getStatus()) {

            case 201:
                return;

            default:
                throw new BankException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Creates a new account in the bank with a full name, id and balance.
     * @param firstName of account holder
     * @param lastName of account holder
     * @param id of account
     * @param balance of account
     * @return
     * @throws BankException
     */
    @Override
    public String createAccount(String firstName, String lastName, String id, BigDecimal balance) throws BankException {

        BankAccountObject bankAccountObject = new BankAccountObject();
        bankAccountObject.firstName = firstName;
        bankAccountObject.lastName = lastName;
        bankAccountObject.id = id;
        bankAccountObject.balance = balance;

        Response response = webTarget
                .path("bank").path("account")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(
                        bankAccountObject,
                        MediaType.APPLICATION_JSON
                ));

        switch (response.getStatus()) {

            case 201:
                return response.readEntity(String.class);

            default:
                throw new BankException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Creates a new account provided by the customer object and a balance.
     * @param customer The customer to create an account for
     * @param amount The intial balance in the account
     * @return the account number of the newly created account.
     * @throws BankException is thrown if the account to be created is not unique.
     */
    @Override
    public String createAccount(Customer customer, BigDecimal amount) throws BankException {
        String[] splitName = customer.getName().split(" ", 2);

        return createAccount(
                splitName[0],
                splitName[1],
                customer.getCpr(),
                amount
        );
    }

    /**
     * Creates a new account provided by the merchant object and a balance.
     * @param merchant The merchant to create an account for
     * @param amount The initial balance in the account
     * @return the account number of the newly created account.
     * @throws BankException is thrown if the account to be created is not unique.
     */
    @Override
    public String createAccount(Merchant merchant, BigDecimal amount) throws BankException {
        String[] splitName = merchant.getName().split(" ", 2);

        return createAccount(
                splitName[0],
                splitName[1],
                merchant.getCvr(),
                amount
        );
    }

    /**
     * Deletes a bank account with the given accountNumber
     * @param accountNumber the accountNumber of the account to be deleted
     * @throws BankException thrown if no account with accountNumber exists.
     */
    @Override
    public void deleteAccount(String accountNumber) throws BankException {
        Response response = webTarget
                .path("bank").path("account").path(accountNumber)
                .request(MediaType.APPLICATION_JSON)
                .delete();

        switch (response.getStatus()) {

            case 200:
                return;

            default:
                throw new BankException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Helper method to get an accounts balance provided by an accountNumber
     * @param accountNumber The accountNumber of the bank account to get the balance from
     * @return a BigDecimal value of the accounts balance
     * @throws BankException If the bank account does not exist.
     */
    @Override
    public BigDecimal getAccountBalance(String accountNumber) throws BankException {
        Response response = webTarget
                .path("bank").path("account").path(accountNumber).path("balance")
                .request(MediaType.APPLICATION_JSON)
                .get();

        switch (response.getStatus()) {

            case 200:
                return response.readEntity(BigDecimal.class);

            default:
                throw new BankException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Gets a list of transactions performed by the account.
     * @param accountNumber the accountNumber of the account.
     * @return list of transactions
     * @throws BankException if the account does not exist.
     */
    @Override
    public List<BankTransactionInterface> getAccountTransactions(String accountNumber) throws BankException {
        Response response = webTarget
                .path("bank").path("account").path(accountNumber).path("transactions")
                .queryParam("accountNumber", accountNumber)
                .request(MediaType.APPLICATION_JSON)
                .get();

        switch (response.getStatus()) {

            case 200:
                return response.readEntity(new GenericType<List<BankTransactionInterface>>() {});

            default:
                throw new BankException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

/*
    @Override
    public String getAccountNumberByCprNumber(String cpr) throws BankException {
        return null;
    }

    @Override
    public String getAccountOwnerName(String accountNumber) throws BankException {
        return null;
    }
*/
}

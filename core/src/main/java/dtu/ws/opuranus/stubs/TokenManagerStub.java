package dtu.ws.opuranus.stubs;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtu.ws.opuranus.Environment;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.BankException;
import dtu.ws.opuranus.exceptions.TokenException;
import dtu.ws.opuranus.interfaces.TokenManagerInterface;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author Anders
 * TokenManagerStubs acts as the interface from the TokenManager to the other micro services of DTUPay
 */
@Service
public class TokenManagerStub implements TokenManagerInterface {
    private ObjectMapper objectMapper;
    private Client client;
    {{
        objectMapper = new ObjectMapper();

        ClientConfig config = new ClientConfig();
        config.register(objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        );

        client = ClientBuilder.newClient();
    }}
    WebTarget webTarget = client.target("http://" + Environment.getHostname() + ":8884/");
    {{
        webTarget.register((ClientRequestFilter) requestContext -> {
            System.out.println(requestContext.getMethod() + " : " + requestContext.getUri());
        });
    }}

    /**
     * Method generateAndSaveToken exposes the generateAndSaveToken from the stub
     * @param customerId integer Customer ID
     * @return Token object
     */
    @Override
    public Token generateAndSaveToken(int customerId) {
        Response response = webTarget
                .path("token")
                .path("generateToken").queryParam("customerId", customerId)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(
                        customerId,
                        MediaType.APPLICATION_JSON));
        if(response.getStatus() == 200){

            return response.readEntity(Token.class);
        }
        throw new RuntimeException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());

    }

    /**
     * getTokenById retrieves a Token based on token ID
     * @param tokenId integer Token ID
     * @return Returns Token
     * @throws TokenException Throws TokenException
     */
    @Override
    public Token getTokenById(Integer tokenId) throws TokenException {

        Response response = webTarget
                .path("token")
                .path("getToken").queryParam("tokenId", tokenId)
                .request(MediaType.APPLICATION_JSON)
                .get();

        switch (response.getStatus()) {

            case 200:
                return response.readEntity(Token.class);

            default:
                throw new RuntimeException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Requests a list of the customer's tokens
     * @param customerId Customer ID as integer
     * @return Returns a List of Tokens
     * @throws TokenException Throws TokenException
     */
    @Override
    public List<Token> requestTokens(int customerId) throws TokenException {
        Response response = webTarget
                .path("token")
                .path("requestTokens")
                .queryParam("customerId", customerId)
                .request(MediaType.APPLICATION_JSON)
                .get();


        if(response.getStatus() == 200){
            return response.readEntity(new GenericType<List<Token>>() {});
        }
        throw new RuntimeException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }

    /**
     * Requests a list of the customer's tokens
     * @param customer A customer object
     * @return Returns a List of Tokens
     * @throws TokenException Throws TokenException
     */
    @Override
    public List<Token> requestTokens(Customer customer) throws TokenException {
        Response response = webTarget
                .path("token")
                .path("requestTokens").queryParam("customerId", customer.getId())
                .request(MediaType.APPLICATION_JSON)
                .get();

        if(response.getStatus() == 200){
            return response.readEntity(new GenericType<List<Token>>() {});
        }
        throw new RuntimeException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }

    /**
     * Requests new tokens for the customer and checks for valid requests
     * @param customerId Customer ID as integer
     * @param max number of Tokens as integer
     * @return Returns a List of Tokens
     * @throws TokenException throws TokenException
     * @throws BankException throws TokenException
     */
    @Override
    public List<Token> requestTokens(int customerId, int max) throws TokenException, BankException {
        Response response = webTarget
                .path("token")
                .path("requestTokens")
                .queryParam("customerId", customerId)
                .queryParam("max", max)
                .request(MediaType.APPLICATION_JSON)
                .get();

        if(response.getStatus() == 200 ){
            return response.readEntity(new GenericType<List<Token>>() {});
        }else if(response.getStatus() == 422){
            throw new TokenException("You can request between 1 and 5 tokens");
        }else if(response.getStatus() == 409) {
            throw new TokenException("You can't request tokens when you have 2 or more tokens");
        }else{
            throw new TokenException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }

    }

    /**
     * Requests tokens
     * @param customer Customer object
     * @param max integer amount of tokens
     * @return Return List of Tokens
     * @throws TokenException Throws TokenException
     * @throws BankException Throws TokenException
     */
    @Override
    public List<Token> requestTokens(Customer customer, int max) throws TokenException, BankException{
        Response response = webTarget
                .path("token")
                .path("requestTokens").queryParam("customerId", customer.getId())
                .queryParam("max", max)
                .request(MediaType.APPLICATION_JSON)
                .get();

        if(response.getStatus() == 200 ){
            return response.readEntity(new GenericType<List<Token>>() {});
        }else if(response.getStatus() == 422){
            throw new BankException("You can request between 1 and 5 tokens");
        }else if(response.getStatus() == 409) {
            throw new TokenException("You can't request tokens when you have 2 or more tokens");
        }else{
            throw new TokenException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Use token
     * @param token Token object
     * @throws TokenException Throws TokenException
     */
    @Override
    public void useToken(Token token) throws TokenException {
        Response response = webTarget
                .path("token")
                .path("useToken").queryParam("token", token)
                .request(MediaType.APPLICATION_JSON)
                .get();

        if(response.getStatus() == 200){
            System.out.println("it works");
        }
        throw new RuntimeException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
    }

    /**
     * Gets number of Tokens to the customer as integer
     * @param customerId Customer ID as integer
     * @return Returns number of tokens as integer
     */
    @Override
    public int getNumberOfTokens(Integer customerId) {
        Response response = webTarget
                .path("token")
                .path("numberOfTokens").queryParam("customerId", customerId)
                .request(MediaType.APPLICATION_JSON)
                .get();

        return response.readEntity(Integer.class);
    }

    /**
     * Get number of Tokens
     * @param customer Customer Object
     * @return Returns number of tokens as integer
     */
    @Override
    public int getNumberOfTokens(Customer customer) {
        Response response = webTarget
                .path("token")
                .path("numberOfTokens").queryParam("customerId", customer.getId())
                .request(MediaType.APPLICATION_JSON)
                .get();

        return response.readEntity(Integer.class);
    }

    /**
     * Get tokens
     * @param customerId Customer ID as integer
     * @return Return List of Tokens
     */
    @Override
    public List<Token> getTokens(Integer customerId) {
        Response response = webTarget
                .path("token")
                .path("getTokens").queryParam("customerId", customerId)
                .request(MediaType.APPLICATION_JSON)
                .get();

        return response.readEntity(new GenericType<List<Token>>() {});
    }

    /**
     * get tokens
     * @param customer Customer object
     * @return List of Tokens
     */
    @Override
    public List<Token> getTokens(Customer customer) {
        Response response = webTarget
                .path("token")
                .path("getTokens").queryParam("customerId", customer.getId())
                .request(MediaType.APPLICATION_JSON)
                .get();

        if(response.getStatus() == 200 || response.getStatus()==201){
            return response.readEntity(new GenericType<List<Token>>() {});
        }
        throw new RuntimeException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());

    }

    /**
     * Get Tokens
     * @param customer Customer object
     * @return Returns a list of tokens
     */
    @Override
    public List<Token> getTokens(Merchant customer) {
        Response response = webTarget
                .path("token")
                .path("getTokens").queryParam("customerId", customer.getId())
                .request(MediaType.APPLICATION_JSON)
                .get();

        return response.readEntity(new GenericType<List<Token>>() {});
    }

    /**
     * Deletes a token
     * @param token Token object to be deleted
     */
    @Override
    public void deleteToken(Token token) {
        Response response = webTarget
                .path("token")
                .path("deleteToken")
                .queryParam("token", token)
                .request()
                .get();


        response.getStatus();

    }
}

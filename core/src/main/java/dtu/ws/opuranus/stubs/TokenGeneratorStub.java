package dtu.ws.opuranus.stubs;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtu.ws.opuranus.Environment;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.interfaces.TokenGeneratorInterface;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Random;

/**
 * @author Anders
 * TokenGeneratorStubs acts as the interface from the TokenGenerator to the other micro services of DTUPay
 */
@Component
//@Primary
public class TokenGeneratorStub
        implements TokenGeneratorInterface {


    protected ObjectMapper objectMapper;
    private Client client;
    {{
        objectMapper = new ObjectMapper();

        ClientConfig config = new ClientConfig();
        config.register(objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        );

        client = ClientBuilder.newClient();
    }}
    WebTarget webTarget = client.target("http://" + Environment.getHostname() + ":8884/");
    {{
        webTarget.register((ClientRequestFilter) requestContext -> {
            System.out.println(requestContext.getUri());
        });
    }}



    private static Random random = new Random();

    public Token generateToken(int identity) {
        Response response = webTarget
                .path("tokenGenerator/generate")
                .queryParam("identity", Integer.toString(identity))
                .request(MediaType.APPLICATION_JSON)
                .get();

        switch (response.getStatus()) {

            case 200:
                return response.readEntity(Token.class);

            default:
                throw new RuntimeException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }
}

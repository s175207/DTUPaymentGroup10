package dtu.ws.opuranus.stubs;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtu.ws.opuranus.Environment;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.exceptions.CustomerException;
import dtu.ws.opuranus.interfaces.CustomerManagerInterface;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/*
 * @Author Daniel
 */
@Service
public class CustomerManagerStub
        implements CustomerManagerInterface {


    protected ObjectMapper objectMapper;
    private Client client;
    {{
        objectMapper = new ObjectMapper();

        ClientConfig config = new ClientConfig();
        config.register(objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        );

        client = ClientBuilder.newClient();
    }}
    WebTarget webTarget = client.target("http://" + Environment.getHostname() + ":8885/");
    {{
        webTarget.register((ClientRequestFilter) requestContext -> {
            System.out.println(requestContext.getMethod() + " : " + requestContext.getUri());
        });
    }}


    /**
     * Creates a customer in the database.
     * @param name Full name of the customer
     * @param cpr Cpr numer of the customer
     * @return The customer object created in the database.
     * @throws CustomerException If the customer already exists.
     */
    @Override
    public Customer createCustomer(String name, String cpr) throws CustomerException {
        Customer customer = new Customer();
        customer.setName(name);
        customer.setCpr(cpr);

        Response response = webTarget
                .path("customer")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(
                        customer,
                        MediaType.APPLICATION_JSON
                ));

        switch (response.getStatus()) {

            case 201:
                return response.readEntity(Customer.class);

            default:
                throw new CustomerException("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Deletes a customer given a customer object
     * @param customer Customer object
     * @throws CustomerException If the customer does not exist
     */
    @Override
    public void deleteCustomer(Customer customer) throws CustomerException {
        Response response = webTarget
                .path("customer")
                .path(Integer.toString(customer.getId()))
                .request()
                .delete();

        if (response.getStatus() == 204) {
            throw new CustomerException("TODO");
        }else if(response.getStatus() == 200){

        }else {
            throw new CustomerException("" + response.getStatus());
        }
    }

    /**
     * Gets a list of customers
     * @return an Iterable of all customers in the database.
     */
    @Override
    public Iterable<Customer> getCustomerList() {
        List<Customer> customers = webTarget
                .path("customer")
                .request()
                .get(new GenericType<List<Customer>>() {});

        return customers;
    }

    /**
     * Returns whether not a customer exists with the given name
     * @param name the name of the customer to search for
     * @return a boolean value whether or not a customer with the name exists.
     */
    @Override
    public boolean hasCustomerByName(String name) {
        Response response = webTarget
                .path("customer")
                .path("searchName")
                .queryParam("name", name)
                .request()
                .get();

        switch (response.getStatus()) {

            case 200:
                Customer customer = response.readEntity(Customer.class);
                return customer != null;

            case 404:
                return false;

            default:
                throw new Error("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Gets whether or not a customer with the cpr number exists
     * @param cpr the cpr number of the customer to search for
     * @return if the customer exist
     */
    @Override
    public boolean hasCustomerByCpr(String cpr) {
        Response response = webTarget
                .path("customer")
                .path("searchCpr")
                .queryParam("cpr", cpr)
                .request()
                .get();

        switch (response.getStatus()) {

            case 200:
                Customer customer = response.readEntity(Customer.class);
                return customer != null;

            case 404:
                return false;

            default:
                throw new Error("TODO: " + response.getStatus() + " - " + response.getStatusInfo().toString());
        }
    }

    /**
     * Gets a customer with the given name
     * @param name the name of the customer
     * @return a Customer object with the given name
     */
    @Override
    public Customer getCustomerByName(String name) {
        Response response = webTarget
                .path("customer").path("searchName")
                .queryParam("name", name)
                .request()
                .get();

        return response.readEntity(Customer.class);
    }

    /**
     * Gets a customer with the given cpr number
     * @param cpr the cpr number to search with
     * @return a Customer object with the given cpr number
     * @throws CustomerException if the customer does not exist
     */
    @Override
    public Customer getCustomerByCpr(String cpr) throws CustomerException {
        Response response = webTarget
                .path("customer").path("searchCpr")
                .queryParam("cpr", cpr)
                .request()
                .get();
        if(response.getStatus() == 200) {
            return response.readEntity(Customer.class);
        } else{
            throw new CustomerException("Customer does not exist");
        }
    }

    /**
     * Gets a customer with the given identity
     * @param identity the identity of the customer
     * @return a Customer object with the given identity
     */
    @Override
    public Customer getCustomerByIdentity(Integer identity) {
        Response response = webTarget
                .path("customer").path(identity.toString())
                .request()
                .get();

        return response.readEntity(Customer.class);
    }

    /**
     * Save a customer object in the database
     * @param customer the customer to be saved to the database
     * @return the customer that was saved.
     */
    @Override
    public Customer saveCustomer(Customer customer) {
        Response response = webTarget
                .path("customer").path(customer.getId().toString())
                .request()
                .put(Entity.entity(
                        customer,
                        MediaType.APPLICATION_JSON
                ));

        return response.readEntity(Customer.class);
    }
}

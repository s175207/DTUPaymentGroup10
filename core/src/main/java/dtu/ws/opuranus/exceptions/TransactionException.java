package dtu.ws.opuranus.exceptions;

/**
 * @author Nicklas
 * handles exceptions on Transaction operations
 */
public class TransactionException extends Exception {
    public TransactionException(String message) {super(message);}
}

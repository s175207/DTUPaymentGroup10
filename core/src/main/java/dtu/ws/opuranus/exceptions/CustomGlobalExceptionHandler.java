package dtu.ws.opuranus.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Nicklas
 * CustomGlobalExceptionHandler catches all specific exception handlers for DTUPay
 * It extends on ResponseEntityExceptionHandler from the Spring framework
 */
@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Catches runtime exceptions
     * @param e Exception parameter input
     * @param response HttpServletResponse parameter
     * @throws IOException Throws IOException on relevant problem
     */
    @ExceptionHandler({ RuntimeException.class })
    public void RuntimeException(Exception e, HttpServletResponse response) throws IOException {
        e.printStackTrace();
        response.sendError(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Merchant: " + e.getMessage()
        );
    }

    /**
     * Catches Bank exceptions. Account already exists, insufficient balance, and account does not exist.
     * @param e BankException parameter input
     * @param response HttpServletResponse parameter
     * @throws IOException Throws IOException on relevant problem
     */
    @ExceptionHandler({ BankException.class })
    public void BankException(BankException e, HttpServletResponse response) throws IOException {

        switch (e.getCause().getMessage()) {

            case "Account already exists":
            case "Debtor balance will be negative":
                response.sendError(
                        HttpStatus.CONFLICT.value(),
                        "Bank: " + e.getCause().getMessage()
                );
                return;

            case "Account does not exist":
                response.sendError(
                        HttpStatus.NOT_FOUND.value(),
                        "Bank: " + e.getCause().getMessage()
                );
                return;

            default:
                response.sendError(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(),
                        "Bank: " + e.getCause().getMessage()
                );
        }
    }

    /**
     * Catches Token exceptions
     * @param e TokenException parameter input
     * @param response HttpServletResponse parameter
     * @throws IOException Throws IOException on relevant problem
     */
    @ExceptionHandler({ TokenException.class })
    public void TokenException(TokenException e, HttpServletResponse response) throws IOException {
        response.sendError(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Token: " + e.getMessage()
        );
    }

    /**
     * Catches Customer exceptions
     * @param e CustomerException parameter input
     * @param response HttpServletResponse parameter
     * @throws IOException Throws IOException on relevant problem
     */
    @ExceptionHandler({ CustomerException.class })
    public void CustomerException(CustomerException e, HttpServletResponse response) throws IOException {
        response.sendError(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Customer: " + e.getMessage()
        );
    }

    /**
     * Catches Merchant exceptions
     * @param e MerchantException parameter input
     * @param response HttpServletResponse parameter
     * @throws IOException Throws IOException on relevant problem
     */
    @ExceptionHandler({ MerchantException.class })
    public void MerchantException(MerchantException e, HttpServletResponse response) throws IOException {
        response.sendError(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Merchant: " + e.getMessage()
        );
    }

    /**
     * Catches Transaction exceptions
     * @param e TokenException parameter input
     * @param response HttpServletResponse parameter
     * @throws IOException Throws IOException on relevant problem
     */
    @ExceptionHandler({ TransactionException.class })
    public void TransactionException(TransactionException e, HttpServletResponse response) throws IOException {
        response.sendError(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Transaction: " + e.getMessage()
        );
    }

    /**
     * Catches EntityNotFound exceptions
     * @param e EntityNotFoundException parameter input
     * @param response HttpServletResponse parameter
     * @throws IOException Throws IOException on relevant problem
     */
    @ExceptionHandler({ EntityNotFoundException.class })
    public void EntityNotFoundException(EntityNotFoundException e, HttpServletResponse response) throws IOException {
        response.sendError(
                HttpStatus.NOT_FOUND.value(),
                e.getMessage()
        );
    }

    /**
     * Catches IllegalArgument exceptions
     * @param e IllegalArgumentException parameter input
     * @param response HttpServletResponse parameter
     * @throws IOException Throws IOException on relevant problem
     */
    @ExceptionHandler({ IllegalArgumentException.class })
    public void IllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
        response.sendError(
                HttpStatus.UNPROCESSABLE_ENTITY.value(),
                e.getMessage()
        );
    }


}

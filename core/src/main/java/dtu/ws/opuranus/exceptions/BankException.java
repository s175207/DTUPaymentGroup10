package dtu.ws.opuranus.exceptions;

/**
 * @author Nicklas
 * BankException handles exceptions on bank operations
 */
public class BankException extends Exception{

    public BankException(String message)
    {
        super(message);
    }
    public BankException(String message, Exception cause)
    {
        super(message, cause);
    }
}

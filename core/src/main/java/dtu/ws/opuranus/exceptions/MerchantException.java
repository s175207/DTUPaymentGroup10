package dtu.ws.opuranus.exceptions;

/**
 * @author Nicklas
 * handles exceptions on Merchant operations
 */
public class MerchantException extends Exception{

    public MerchantException(String message)
    {
        super(message);
    }
}

package dtu.ws.opuranus.exceptions;

/**
 * @author Nicklas
 * CustomerException handles exceptions on Customer operations
 */
public class CustomerException extends Exception{

    public CustomerException(String message)
    {
        super(message);
    }
}

package dtu.ws.opuranus.exceptions;

/**
 * @author Nicklas
 * handles exceptions on Token operations
 */
public class TokenException extends Exception{

    public TokenException(String message)
    {
        super(message);
    }
}

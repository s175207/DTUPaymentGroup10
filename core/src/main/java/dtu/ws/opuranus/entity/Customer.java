package dtu.ws.opuranus.entity;

import javax.persistence.*;


/**
 * @author Nicklas
 * Customer entity - the programmatic representation of the system's end-user
 */
@Entity
public class Customer implements UserInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    @Column(unique = true)
    String name;

    @Column(unique = true)
    String cpr;

    @Column(unique = true)
    String accountNumber;

    /**
     * Customer constructor
     */
    public Customer() {
    }

    /**
     * Customer object in DTUPay system. ID is generated.
     * @param name The full name of the customer
     * @param cpr CPR value of the customer
     * @param accountNumber AccountNumber of the customer to be used in the bank.
     */
    public Customer(String name, String cpr, String accountNumber) {
        this.name = name;
        this.cpr = cpr;
        this.accountNumber = accountNumber;
    }

    /**
     * Customer object in DTUPay system with a custom ID.
     * @param id ID to uniquely identify the customer in the database.
     * @param name The full name of the customer
     * @param cpr CPR value of the customer
     * @param accountNumber AccountNumber of the customer to be used in the bank.
     */
    public Customer(Integer id, String name, String cpr, String accountNumber) {
        this.id = id;
        this.name = name;
        this.cpr = cpr;
        this.accountNumber = accountNumber;
    }

    /**
     * Getter for ID
     * @return ID of the customer.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for ID
     * @param id ID of the customer
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter for Name
     * @return name of the customer
     */
    public String getName() {
        return name;
    }
    /**
     * Setter for Name
     * @param name Name of the customer
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for CPR number
     * @return the CPR number of the customer
     */
    public String getCpr() {
        return cpr;
    }

    /**
     * Setter for CPR number
     * @param cpr the cpr number of the customer
     */
    public void setCpr(String cpr) {
        this.cpr = cpr;
    }

    /**
     * Getter for the Account Number
     * @return the account number of the customer
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Setter for the Account number
     * @param accountNumber the account number of the custoemr
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}

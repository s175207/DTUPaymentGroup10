package dtu.ws.opuranus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dtu.ws.opuranus.exceptions.TokenException;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;


/**
 * @author Nicklas
 * Token entity
 */
@Entity
public class Token {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private Integer identity;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created")
    private Date issued = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "invalidated")
    private Date invalidated;

    @Column
    private Integer merchantId;

    @Column
    private Integer usedToken;


    @JsonIgnore
    public boolean isValid() {
        return invalidated == null;
    }

    public String toString() {
        return "Token [ Identity: " + getIdentity() + " | Issued: " + getIssued() + " | Invalidated: " + getInvalidated() + " ]";
    }

    /**
     * Invalidates a token by setting the Date invalidated to the current time.
     * @throws TokenException If the token is already invalidated a TokenException is thrown.
     */
    public void invalidate() throws TokenException {
        if (! isValid()) {
            throw new TokenException("Token has already been invalidated");
        }
        setInvalidated(new Date());
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdentity() {
        return identity;
    }

    public void setIdentity(Integer identity) {
        this.identity = identity;
    }

    public Date getIssued() {
        return issued;
    }

    public void setIssued(Date issued) {
        this.issued = issued;
    }

    public Date getInvalidated() {
        return invalidated;
    }

    public void setInvalidated(Date invalidated) {
        this.invalidated = invalidated;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public Integer getUsedToken() {
        return usedToken;
    }

    public void setUsedToken(Integer usedToken) {
        this.usedToken = usedToken;
    }
}

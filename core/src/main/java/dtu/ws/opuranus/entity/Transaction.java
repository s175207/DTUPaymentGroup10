package dtu.ws.opuranus.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Mathias
 * Transaction entity
 */
@Entity
public class Transaction {
    @Override
    public String toString() {
        return  transactionTime.toString() +
                ", " + saleId +
                ", " + customerId +
                ", " + merchantId +
                ", " + transactionAmount.toString() +
                ", " + ((refunded==null) ? "-" : refunded);
    }

    @Id
    private Integer saleId;

    @Column
    private BigDecimal transactionAmount;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date transactionTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date refunded;

    private int customerId;

    private int merchantId;

    /**
     * Empty constructor to be used for serialization
     */
    public Transaction() {
    }

    /**
     * Constructor for a transaction containing the customer and merchant, the token used and the price of the transaction.
     * @param customer The customer buying something.
     * @param merchant The merchant selling something
     * @param salesToken The token used to perform the sale
     * @param price The price of the item sold
     */
    public Transaction(Customer customer, Merchant merchant, Token salesToken, BigDecimal price){
        this.saleId = salesToken.getId();
        this.customerId = customer.getId();
        this.merchantId = merchant.getId();
        this.transactionAmount = price;
    }

    public Integer getSaleId() {
        return saleId;
    }

    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Date getRefunded() {
        return refunded;
    }

    public void setRefunded(Date refunded) {
        this.refunded = refunded;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerid) {
        this.customerId = customerid;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Merchant merchant) {
        this.merchantId = merchant.getId();
    }
}

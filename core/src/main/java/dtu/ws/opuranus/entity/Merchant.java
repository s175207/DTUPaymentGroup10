package dtu.ws.opuranus.entity;

import javax.persistence.*;

/**
 * @author Nicklas
 * Merchant entity
 */
@Entity
public class Merchant implements UserInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    @Column(unique = true)
    String name;

    @Column(unique = true)
    String cvr;

    @Column(unique = true)
    String accountNumber;

    /**
     * Empty Constructor to be used for serialization.
     */
    public Merchant() {
    }

    /**
     * Constructor for creating a new Merchant object with a generated ID.
     * @param name The full name of the Merchant
     * @param cvr The cvr number of the merchants company
     * @param accountNumber The merchants account number in the bank.
     */
    public Merchant(String name, String cvr, String accountNumber) {
        this.name = name;
        this.cvr = cvr;
        this.accountNumber = accountNumber;
    }

    /**
     * Constructor for creating a new merchant with a chosen ID.
     * @param id The id of the merchant. Must be unique.
     * @param name The full name of the Merchant
     * @param cvr T he cvr number of the merchants company
     * @param accountNumber The merchants account number in the bank.
     */
    public Merchant(Integer id, String name, String cvr, String accountNumber) {
        this.id = id;
        this.name = name;
        this.cvr = cvr;
        this.accountNumber = accountNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCvr() {
        return cvr;
    }

    public void setCvr(String cvr) {
        this.cvr = cvr;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}

package dtu.ws.opuranus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Mathias
 */
@SpringBootApplication(
        scanBasePackages = {
                "dtu.ws.opuranus"
        }
)
public class PaymentApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaymentApplication.class, args);

    }
}

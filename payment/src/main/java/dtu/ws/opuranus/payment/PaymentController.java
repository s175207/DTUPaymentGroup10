package dtu.ws.opuranus.payment;

import dtu.ws.opuranus.PaymentObject;
import dtu.ws.opuranus.interfaces.BankAdapterInterface;
import dtu.ws.opuranus.interfaces.ReportManagerInterface;
import dtu.ws.opuranus.exceptions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mathias
 * PaymentController is REST controller between DTUPay and the transaction database.
 * The class handles database connections, database mapping, and object-relational mapping.
 */
@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    BankAdapterInterface bankAdapter;

    @Autowired
    PaymentGateway paymentGateway;

    @Autowired
    ReportManagerInterface reportManager;

    /**
     * Create a transaction record in the database
     * @param paymentObject Paymentobject (a wrapper for payments)
     * @return Returns a responseEntity object containing the transaction ID
     */
    @PostMapping("/transfer")
    public ResponseEntity transfer(
           @RequestBody PaymentObject paymentObject
     ) {
        try {
            System.out.print("MerchantaccountNumber: " + paymentObject.from.getAccountNumber());
            System.out.print("  MerchantCVR: " + paymentObject.from.getCvr());
            System.out.print("  nMerchantID: " + paymentObject.from.getId());
            System.out.println("  MerchantName " + paymentObject.from.getName());


            System.out.println("Description: " + paymentObject.description);

            System.out.println("TokenID: " + paymentObject.tokenId);

            System.out.println("Amount: " + paymentObject.amount);




            Integer transactionId = paymentGateway.doTransaction(paymentObject.from, paymentObject.tokenId, paymentObject.amount, paymentObject.description);

            return ResponseEntity
                    .ok(transactionId);

        } catch (CustomerException | MerchantException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("TODO...: " + e.getMessage());

        } catch (TokenException e) {
            e.printStackTrace();

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Token: " + e.getMessage());

        } catch (BankException e) {

            if (e.getCause() != null) {
                switch (e.getCause().getMessage()) {

                    case "Debtor account does not exist":
                        return ResponseEntity
                                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                                .body("Token: " + e.getMessage());
                }
            }

            e.printStackTrace();

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Bank: " + e.getMessage());

        }
    }



    /**
     * Refund a transaction record in the database
     * @param transactionId Paymentobject (a wrapper for payments)
     * @param description String description of the refunded transaction
     * @return Returns a responseEntity object containing nothing but possible exception messages
     */
    @GetMapping("/refund")
        public ResponseEntity refund(
            @Param("transactionId") int transactionId,
            @Param("description") String description
    ) {
        try {
            paymentGateway.doRefund(transactionId, description);

            return ResponseEntity
                    .noContent()
                    .build();

        } catch (CustomerException | MerchantException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("TODO...: " + e.getMessage());

        } catch (BankException e) {
            e.printStackTrace();

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Bank: " + e.getMessage());

        } catch (TransactionException transactionException) {
            transactionException.printStackTrace();

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Transaction: " + transactionException.getMessage());
        }
    }
}

package dtu.ws.opuranus.payment;

import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.entity.Transaction;
import dtu.ws.opuranus.exceptions.*;
import dtu.ws.opuranus.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author Mathias
 * PaymentGateway handles creation and "refunding" of Transaction objects inside the DTUPay webservice.
 */
@Service
@Primary
public class PaymentGateway implements PaymentGatewayInterface {

    @Autowired
    private BankAdapterInterface bankAdapter;

    @Autowired
    private ReportManagerInterface reportManager;

    @Autowired
    private CustomerManagerInterface customerManager;

    @Autowired
    private MerchantManagerInterface merchantManager;

    @Autowired
    private TokenManagerInterface tokenManager;

    /**
     * Initializes transactions and registers them in the DTUPay logs
     * @param merchant Merchant object
     * @param tokenId ID of the token used for the transaction
     * @param amount Monetary amount spent in the transaction
     * @param description Description of the transaction
     * @return Returns the ID of the transaction as an integer
     * @throws CustomerException CustomerException is thrown at unhandled exceptions using the customerManager
     * @throws MerchantException MerchantException is thrown at unhandled exceptions using the merchantManager
     * @throws TokenException TokenException is thrown at unhandled exceptions using the tokenManager
     * @throws BankException BankException is thrown at unhandled exceptions using the bankAdapter
     */
    public int doTransaction(Merchant merchant, Integer tokenId, BigDecimal amount, String description) throws CustomerException, MerchantException, TokenException, BankException {
        Token token = tokenManager.getTokenById(tokenId);

        token.invalidate();

        Customer customer = customerManager.getCustomerByIdentity(token.getIdentity());
        System.out.println("Hvad har vi her? " + customer.getCpr());

        bankAdapter.transfer(
                customer.getAccountNumber(),
                merchant.getAccountNumber(),
                amount,
                description
        );
        Transaction transaction = reportManager.logNewTransaction(customer, merchant, token, amount);

        return transaction.getSaleId();
    }

    /**
     * Do a refund of a transaction
     * @param transactionId Integer ID of the transaction to refund
     * @param description String description of the transaction
     * @return Returns a string with the result of the process
     * @throws CustomerException CustomerException is thrown at unhandled exceptions using the customerManager
     * @throws MerchantException MerchantException is thrown at unhandled exceptions using the merchantManager
     * @throws TransactionException TokenException is thrown at unhandled exceptions using the tokenManager
     * @throws BankException BankException is thrown at unhandled exceptions using the bankAdapter
     */
    public String doRefund(int transactionId, String description) throws CustomerException, MerchantException, BankException, TransactionException {
        Merchant merchant;
        Customer customer;
        BigDecimal amountToRefund;

        Transaction transaction = reportManager.getTransactionById(transactionId);

        if (transaction.getRefunded() == null) {
            merchant = merchantManager.getMerchantByIdentity(transaction.getMerchantId());
            customer = customerManager.getCustomerByIdentity(transaction.getCustomerId());
            amountToRefund = transaction.getTransactionAmount();
        } else {
            throw new TransactionException("Transaction already refunded at: " + transaction.getRefunded());
        }
        bankAdapter.transfer(merchant.getAccountNumber(), customer.getAccountNumber(), amountToRefund, description);

        reportManager.markRefunded(transaction);

        return "Refund succeed";
    }
}

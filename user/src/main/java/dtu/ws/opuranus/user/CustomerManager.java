package dtu.ws.opuranus.user;

import dtu.ws.opuranus.exceptions.MerchantException;
import dtu.ws.opuranus.interfaces.CustomerManagerInterface;
import dtu.ws.opuranus.interfaces.PaymentGatewayInterface;
import dtu.ws.opuranus.interfaces.ReportManagerInterface;
import dtu.ws.opuranus.interfaces.TokenManagerInterface;
import dtu.ws.opuranus.exceptions.CustomerException;
import org.springframework.beans.factory.annotation.Autowired;

import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.TokenException;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Daniel
 * CustomerManager handles CRUD responsibilities for all Customer objects inside the DTUPay webservice
 */
@Service
@Primary
public class CustomerManager
        implements CustomerManagerInterface {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private TokenManagerInterface tokenManager;

    /**
     * Creates a new Customer object
     * @param name Name of the new Customer object
     * @param cpr CPR number of the new Customer object
     * @return Returns the new Customer object
     */
    public Customer createCustomer(String name, String cpr) {
        Customer customer = new Customer();
        customer.setName(name);
        customer.setCpr(cpr);
        customerRepository.save(customer);
        return customer;
    }

    /**
     * Deletes a Customer object
     * @param customer The Merchant object to be deleted
     */
    public void deleteCustomer(Customer customer) {
        for (Token token : tokenManager.getTokens(customer)) {
            try {
                token.invalidate();

            } catch (TokenException e) {
                e.printStackTrace();
                throw new Error("TODO...");
            }
        }

        customerRepository.delete(customer);
    }

    /**
     * Get list of all Customer objects
     * @return Returns an iterable list of all Customer objects
     */
    public List<Customer> getCustomerList() {
        return customerRepository.findAll();
    }

    /**
     * Check if a named Customer object exists
     * @param name Name of the Customer object as a string
     * @return returns true if the Customer object exists
     */
    public boolean hasCustomerByName(String name) {
        return customerRepository.findOneByName(name).isPresent();
    }

    /**
     * Check if a Customer object exists.
     * [Overload not implemented correctly]
     * @param cpr CPR of the Merchant object as a string
     * @return returns true if the Merchant object exists
     */
    public boolean hasCustomerByCpr(String cpr) {
        return customerRepository.findOneByCpr(cpr).isPresent();
    }

    @Override
    public Customer getCustomerByName(String name) throws CustomerException {
        Optional<Customer> customer = customerRepository.findOneByName(name);
        customer.orElseThrow(() -> new CustomerException("No customer with the name " + name));
        return customer.get();
    }

    /**
     * Get a Customer object based on it's cpr.
     * @param cpr ID of the requested Customer.
     * @return Returns the Customer object if it exists.
     * @throws CustomerException Throws a CustomerException if the process encounters an unexpected Exception
     */
    @Override
    public Customer getCustomerByCpr(String cpr) throws CustomerException {
        Optional<Customer> customer = customerRepository.findOneByCpr(cpr);
        customer.orElseThrow(() -> new CustomerException("No customer with cpr " + cpr));
        return customer.get();
    }

    /**
     * Get a Customer object based on it's ID.
     * @param identity ID of the requested Customer.
     * @return Returns the Customer object if it exists.
     * @throws CustomerException Throws a CustomerException if the process encounters an unexpected Exception
     */
    public Customer getCustomerByIdentity(Integer identity) throws CustomerException {
        Optional<Customer> customer = customerRepository.findById(identity);
        customer.orElseThrow(() -> new CustomerException("No customer with the id " + identity));
        return customer.get();
    }

    /**
     * Edit Customer object
     * @param customer The Customer object to edit
     * @return Returns a customerRepository.save function
     */
    @Override
    public Customer saveCustomer(Customer customer) throws CustomerException {
        return customerRepository.save(customer);
    }
}

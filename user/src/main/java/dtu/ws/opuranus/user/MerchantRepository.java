package dtu.ws.opuranus.user;

import dtu.ws.opuranus.entity.Merchant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Daniel
 * MerchantRepository is the class used by DTUPay to handle merchants in the database.
 * MerchantRepository extends CrudRepository giving the class Create, Read, Update, Delete functionalities.
 */
@Repository
public interface MerchantRepository extends CrudRepository<Merchant, Integer> {

    /**
     * Retrieve all Merchant objects from database
     * @return returns a list of all Merchant objects from database
     */
    List<Merchant> findAll();

    /**
     * Retrieve one Merchant with [ID] from database
     * @param id integer identifying a specific Merchant object
     * @return returns (if found) a Merchant object from database
     */
    Optional<Merchant> findById(@Param("id") Integer id);

    /**
     * Retrieve one Merchant with [name] from database
     * @param name name as a string identifying a specific Merchant object
     * @return returns the requested Merchant object if one is found
     */
    Optional<Merchant> findOneByName(@Param("name") String name);

    /**
     * Retrieve one Merchant with [CVR] from database
     * @param cvr CVR number as a string identifying a specific Merchant object
     * @return returns one or more Merchant objects if one is found
     */
    Optional<Merchant> findOneByCvr(@Param("cvr") String cvr);

    /**
     * Retrieve Merchant from database using both [name] and [cvr] number
     * Used in case of naming collisions
     * @param name name as a string identifying a specific Merchant object
     * @param cvr CVR number as a string identifying a specific Merchant object
     * @return returns the requested Merchant object if one is found
     */
    Optional<Merchant> findOneByNameAndCvr(@Param("name") String name, @Param("cvr") String cvr);

    /**
     * Retrieve all Merchants from database using [name] as identifier
     * @param name name as a string identifying a specific Merchant object
     * @return returns the requested Merchant objects if found
     */
    List<Merchant> findByName(@Param("name") String name);

    /**
     * Retrieve all Merchants from database using [cpr] as identifier
     * @param cpr CPR number as a string identifying a specific Merchant object
     * @return returns the requested Merchant objects if found
     */
    List<Merchant> findByCvr(@Param("cpr") String cpr);

    /**
     * Retrieve all Merchants from database using [name] and [cpr] as identifier
     * Used in case of naming collisions
     * @param name The name of the Merchant object
     * @param cpr CPR number as a string identifying a specific Merchant object
     * @return Returns a list of Merchant objects
     */
    List<Merchant> findByNameAndCvr(@Param("name") String name, @Param("cpr") String cpr);
}

package dtu.ws.opuranus.user;

import dtu.ws.opuranus.interfaces.CustomerManagerInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.exceptions.CustomerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.NotFoundException;
import java.util.List;

/**
 * @author Daniel
 *  * CustomerController acts as the link between DTUPay and the Customer database..
 *  * The class handles database connections, database mapping, and object-relational mapping.
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerManagerInterface customerManager;

    /**
     * getCustomers retrieves a list of Customer objects from the database.
     * @return Returns a list of Customer objects
     */
    @GetMapping({ "", "/" })
    public List<Customer> getCustomers() {
        return (List<Customer>) customerManager.getCustomerList();
    }

    /**
     * Creates a new record in the Customer database
     * @param newCustomer The Customer object to be mapped into the database.
     * @return Returns a ResponseEntity from the spring framework.
     *  The response is a http-status of the record creation and the Customer object.
     */
    @PostMapping({ "", "/" })
    public ResponseEntity<Customer> createCustomer(
            @RequestBody Customer newCustomer
    ) {
        try {
            Customer customer = customerManager.createCustomer(
                    newCustomer.getName(),
                    newCustomer.getCpr()
            );

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(customer);

        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }

    /**
     * Retrieve a Customer from database.
     * @param customerId Search param is the customerId of the Customer object
     * @return ResponseEntity consisting of a Customer object and a http reply
     */
    @GetMapping("/{customerId}")
    public ResponseEntity getCustomerWithId(
            @PathVariable Integer customerId
    ) {
        try {
            Customer customer = customerManager.getCustomerByIdentity(customerId);
            if (customer == null) {
                throw new NotFoundException("No user with the given ID exists");
            }
            return ResponseEntity
                    .ok(customer);

        } catch (CustomerException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }

    /**
     * Edit a Customer in database.
     * @param customerId Search param is the customerId
     * @return ResponseEntity consisting of the new Customer object and a http reply
     */
    @PutMapping("/{customerId}")
    public ResponseEntity replaceCustomer(
            @RequestBody Customer newCustomer,
            @PathVariable Integer customerId
    ) {
        try {
            Customer customer = customerManager.getCustomerByIdentity(customerId);
            customer.setName(newCustomer.getName());
            customer.setCpr(newCustomer.getCpr());
            customer.setAccountNumber(newCustomer.getAccountNumber());
            customer = customerManager.saveCustomer(customer);

            return ResponseEntity
                    .ok()
                    .body(customer);

        } catch (CustomerException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }

    /**
     * Delete a Customer in database.
     * @param customerId Search param is the customerId
     * @return ResponseEntity consisting of a http reply
     */
    @DeleteMapping("/{customerId}")
    public ResponseEntity deleteCustomer(
            @PathVariable("customerId") Integer customerId
    ) {
        try {
            Customer customer = customerManager.getCustomerByIdentity(customerId);
            customerManager.deleteCustomer(customer);

            return ResponseEntity
                    .ok()
                    .body(null);

        } catch (CustomerException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }

    /**
     * Retrieve Customer from database using [name]
     * @param name name as a string identifying a specific Customer object
     * @return returns the requested Customer object if one is found
     */
    @GetMapping("/searchName")
    public ResponseEntity oneByName(
            @Param("name") String name
    ) {
        try {
            Customer customer = customerManager.getCustomerByName(name);

            return ResponseEntity
                    .ok()
                    .body(customer);

        } catch (CustomerException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(null);
        }
    }

    /**
     * Retrieve Customer from database using [Cpr]
     * @param cpr CPR number as a string identifying a specific Customer object
     * @return returns the requested Customer object if one is found
     */
    @GetMapping("/searchCpr")
    public ResponseEntity oneByCpr(
            @Param("cpr") String cpr
    ) {
       try {
           Customer customer = customerManager.getCustomerByCpr(cpr);

           return ResponseEntity
                   .ok()
                   .body(customer);

       } catch (CustomerException e) {
           return ResponseEntity
                   .status(HttpStatus.NOT_FOUND)
                   .body(null);
       }
    }
}

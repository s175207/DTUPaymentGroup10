package dtu.ws.opuranus.user;

import dtu.ws.opuranus.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Daniel
 * CustomerRepository is the class used by DTUPay to handle customers in the database.
 * CustomerRepository extends CrudRepository giving the class Create, Read, Update, Delete functionalities.
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    /**
     * Retrieve all Customer objects from database
     * @return returns a list of all Customer objects from database
     */
    List<Customer> findAll();

    /**
     * Retrieve one Customer with [ID] from database
     * @param id integer identifying a specific Customer object
     * @return returns (if found) a Customer object from database
     */
    Optional<Customer> findById(@Param("id") Integer id);

    /**
     * Retrieve one Customer with [name] from database
     * @param name name as a string identifying a specific Customer object
     * @return returns the requested Customer object if one is found
     */
    Optional<Customer> findOneByName(@Param("name") String name);

    /**
     * Retrieve one Customer with [CPR] from database
     * @param cpr CPR number as a string identifying a specific Customer object
     * @return returns one or more Customer objects if one is found
     */
    Optional<Customer> findOneByCpr(@Param("cpr") String cpr);

    /**
     * Retrieve Customer from database using both [name] and [cpr] number
     * Used in case of naming collisions
     * @param name name as a string identifying a specific Customer object
     * @param cpr CPR number as a string identifying a specific Customer object
     * @return returns the requested Merchant object if one is found
     */
    Optional<Customer> findOneByNameAndCpr(@Param("name") String name, @Param("cpr") String cpr);
}

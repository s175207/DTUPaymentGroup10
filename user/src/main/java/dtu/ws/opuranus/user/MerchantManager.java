package dtu.ws.opuranus.user;

import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.MerchantException;
import dtu.ws.opuranus.exceptions.TokenException;
import dtu.ws.opuranus.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Daniel
 * MerchantManager handles CRUD responsibilities for all Merchant objects inside the DTUPay webservice
 */
@Service
@Primary
public class MerchantManager
        implements MerchantManagerInterface {

    @Autowired
    private MerchantRepository merchantRepository;


    @Autowired
    private TokenManagerInterface tokenManager;


    /**
     * Creates a new Merchant object
     * @param name Name of the new Merchant object
     * @param cpr CPR number of the new Merchant object
     * @return Returns the new Merchant object
     */
    @Override
    public Merchant createMerchant(String name, String cpr) {
        Merchant merchant = new Merchant();
        merchant.setName(name);
        merchant.setCvr(cpr);
        merchantRepository.save(merchant);
        return merchant;
    }

    /**
     * Deletes a Merchant object
     * @param merchant The Merchant object to be deleted
     */
    @Override
    public void deleteMerchant(Merchant merchant) {
        for (Token token : tokenManager.getTokens(merchant)) {
            try {
                token.invalidate();

            } catch (TokenException e) {
                e.printStackTrace();
                throw new Error("TODO...");
            }
        }

        merchantRepository.delete(merchant);
    }

    /**
     * Get list of all Merchant objects
     * @return Returns an iterable list of all Merchant objects
     */
    @Override
    public Iterable<Merchant> getMerchantList() {
        return merchantRepository.findAll();
    }

    /**
     * Check if a named Merchant object exists
     * @param name Name of the Merchant object as a string
     * @return returns true if the Merchant object exists
     */
    @Override
    public boolean hasMerchantByName(String name) {
        {
            return merchantRepository.findByName(name).size() > 0;
        }
    }

    /**
     * Check if a Merchant object exists.
     * [Overload not implemented correctly]
     * @param cvr CVR of the Merchant object as a string
     * @return returns true if the Merchant object exists
     */
    @Override
    public boolean hasMerchantByCvr(String cvr) {
        return merchantRepository.findByCvr(cvr).size() > 0;
    }

    /**
     * Get a Merchant object based on it's ID.
     * @param identity ID of the requested Merchant.
     * @return Returns the Merchant object if it exists.
     * @throws MerchantException Throws a MerchantException if the process encounters an unexpected Exception
     */
    @Override
    public Merchant getMerchantByIdentity(Integer identity) throws MerchantException {
        Optional<Merchant> merchant = merchantRepository.findById(identity);
        merchant.orElseThrow(() -> new MerchantException("No customer with the id " + identity));
        return merchant.get();
    }

    /**
     * Edit Merchant object
     * @param merchant The Merchant object to edit
     * @return Returns a merChantRepository.save function
     */
    @Override
    public Merchant saveMerchant(Merchant merchant) {
        return merchantRepository.save(merchant);
    }

    /**
     * Get a Merchant object
     * @param name Name of the Merchant object to retrieve
     * @return Returns a Merchant object
     */
    @Override
    public Merchant getMerchantByName(String name) {
        System.out.println("TODO");
        throw new Error("TODO");
    }

    /**
     * Get a Merchant object
     * @param cvr CVR of the Merchant object to retrieve
     * @return Returns a Merchant object
     */
    @Override
    public Merchant getMerchantByCvr(String cvr) {
        System.out.println("TODO");
        throw new Error("TODO");
    }
}

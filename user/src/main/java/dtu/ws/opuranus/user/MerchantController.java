package dtu.ws.opuranus.user;

import dtu.ws.opuranus.interfaces.MerchantManagerInterface;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.MerchantException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.NotFoundException;
import java.util.List;

/**
 * @author Daniel
 * MerchantController acts as the link between DTUPay and the Merchant database..
 * The class handles database connections, database mapping, and object-relational mapping.
 */
@RestController
@RequestMapping("/merchant")
public class MerchantController {

    @Autowired
    MerchantManagerInterface merchantManager;

    /**
     * getMerchants retrieves a list of Merchant objects from the database.
     * @return Returns a list of Merchant objects
     */
    @GetMapping({ "", "/" })
    public List<Merchant> getMerchants() {
        return (List<Merchant>) merchantManager.getMerchantList();
    }

    /**
     * Creates a new record in the Merchant database
     * @param newMerchant The Merchant object to be mapped into the database.
     * @return Returns a ResponseEntity from the spring framework.
     *  The response is a http-status of the record creation and the Merchant object.
     */
    @PostMapping({ "", "/" })
    public ResponseEntity<Merchant> createMerchant(
            @RequestBody Merchant newMerchant
    ) {
        try {
            Merchant merchant = merchantManager.createMerchant(
                    newMerchant.getName(),
                    newMerchant.getCvr()
            );

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(merchant);

        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }

    /**
     * Retrieve a Merchant from database.
     * @param merchantId Search param is the merchantId
     * @return ResponseEntity consisting of a Merchant object and a http reply
     */
    @GetMapping("/{merchantId}")
    public ResponseEntity getMerchantWithId(
            @PathVariable Integer merchantId
    ) {
        try {
            Merchant merchant = merchantManager.getMerchantByIdentity(merchantId);
            if (merchant == null) {
                throw new NotFoundException("No user with the given ID exists");
            }
            return ResponseEntity
                    .ok(merchant);

        } catch (MerchantException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }

    /**
     * Edit a Merchant in database.
     * @param merchantId Search param is the merchantId
     * @return ResponseEntity consisting of the new Merchant object and a http reply
     */
    @PutMapping("/{merchantId}")
    public ResponseEntity replaceMerchant(
            @RequestBody Merchant newMerchant,
            @PathVariable Integer merchantId
    ) {
        try {
            Merchant merchant = merchantManager.getMerchantByIdentity(merchantId);
            merchant.setName(newMerchant.getName());
            merchant.setCvr(newMerchant.getCvr());
            merchant.setAccountNumber(newMerchant.getAccountNumber());
            merchant = merchantManager.saveMerchant(merchant);

            return ResponseEntity
                    .ok()
                    .body(merchant);

        } catch (MerchantException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }

    /**
     * Delete a Merchant in database.
     * @param merchantId Search param is the merchantId
     * @return ResponseEntity consisting of a http reply
     */
    @DeleteMapping("/{merchantId}")
    public ResponseEntity deleteMerchant(
            @PathVariable("merchantId") Integer merchantId
    ) {
        try {
            Merchant merchant = merchantManager.getMerchantByIdentity(merchantId);
                merchantManager.deleteMerchant(merchant);

            return ResponseEntity
                    .ok()
                    .body(null);

        } catch (MerchantException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }


    /**
     * Retrieve Merchant from database using both [name] and [cvr] number
     * @param name name as a string identifying a specific Merchant object
     * @param cvr CVR number as a string identifying a specific Merchant object
     * @return returns the requested Merchant object if one is found
     */
    @GetMapping("/search")
    public Merchant oneByNameAndCpr(
            @Param("name") String name,
            @Param("cvr") String cvr
    ) {
        throw new Error("TODO");
    }

    /**
     * Retrieve Merchant record using [name] as identifier
     * @param name name as a string identifying a specific Merchant object
     * @return returns the requested Merchant object if found
     */
    @GetMapping("/searchName")
    public ResponseEntity oneByName(
            @Param("name") String name
    ) {
        try {
            Merchant merchant = merchantManager.getMerchantByName(name);

            return ResponseEntity
                    .ok()
                    .body(merchant);

        } catch (MerchantException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(null);
        }
    }

    /**
     * Retrieve Merchant record using [cvr] as identifier
     * @param cvr name as a string identifying a specific Merchant object
     * @return returns the requested Merchant object if found
     */
    @GetMapping("/searchCvr")
    public ResponseEntity oneByCvr(
            @Param("cvr") String cvr
    ) {
       try {
           Merchant merchant = merchantManager.getMerchantByCvr(cvr);

           return ResponseEntity
                   .ok()
                   .body(merchant);

       } catch (MerchantException e) {
           return ResponseEntity
                   .status(HttpStatus.NOT_FOUND)
                   .body(null);
       }
    }
}

package dtu.ws.opuranus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rasmus
 */
@SpringBootApplication(
        scanBasePackages = {
                "dtu.ws.opuranus"
        }
)
public class MonolithApplication {
    public static void main(String[] args) {
        SpringApplication.run(MonolithApplication.class, args);

    }
}

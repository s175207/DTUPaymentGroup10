package dtu.ws.opuranus.bank;

import dtu.ws.opuranus.interfaces.BankAdapterInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.BankException;

import java.math.BigDecimal;

/**
 * @author Heino
 */
abstract public class AbstractBankAdapter
        implements BankAdapterInterface {

    /**
     * Creates a new account in the bank
     * @param customer Customer object
     * @param amount BigDecimal amount
     * @return AccountNumber of the newly created account
     * @throws BankException if the account already exists.
     */
    @Override
    public String createAccount(Customer customer, BigDecimal amount) throws BankException {

        String[] splitName = customer.getName().split(" ", 2);

        return createAccount(
                splitName[0],
                splitName[1],
                customer.getCpr(),
                amount
        );
    }

    /**
     * Creates a new account in the bank
     * @param merchant Customer object
     * @param amount BigDecimal amount
     * @return String with AccountNumber of newly created account
     * @throws BankException if he account already exists.
     */
    @Override
    public String createAccount(Merchant merchant, BigDecimal amount) throws BankException {

        String[] splitName = merchant.getName().split(" ", 2);

        return createAccount(
                splitName[0],
                splitName[1],
                merchant.getCvr(),
                amount
        );
    }
}

package dtu.ws.opuranus.bank;

import dtu.ws.opuranus.BankAccountObject;
import dtu.ws.opuranus.interfaces.BankTransactionInterface;
import dtu.ws.opuranus.TransferObject;
import dtu.ws.opuranus.interfaces.BankAdapterInterface;
import dtu.ws.opuranus.exceptions.BankException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Heino
 */
@RestController
@RequestMapping("/bank")
public class BankController {

    @Autowired
    BankAdapterInterface bankAdapter;

    /**
     * Takes a post request and performs a transfer between a customer and a merchant
     * @param transferObject object that includes sender and receiver, amount and a description.
     * @return a response with a status code and possibly a body with an error message
     */
    @PostMapping("/transfer")
    public ResponseEntity transfer(
            @RequestBody TransferObject transferObject
    ) {
        try {
            bankAdapter.transfer(
                    transferObject.from,
                    transferObject.to,
                    transferObject.amount,
                    transferObject.description
            );

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(null);

        } catch (BankException e) {

            switch (e.getCause().getMessage()) {

                case "Debtor balance will be negative":
                    return ResponseEntity
                            .status(HttpStatus.CONFLICT)
                            .body("Bank: " + e.getMessage());

                case "Amount must be positive":
                case "Description missing":
                    return ResponseEntity
                            .status(HttpStatus.BAD_REQUEST)
                            .body("Bank: " + e.getMessage());

            }
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Bank: " + e.getMessage());
        }
    }

    /**
     * Create an account in the bank
     * @param bankAccountObject contains full name, id and initial account balance
     * @return the account number of the newly created account.
     */
    @PostMapping("/account")
    public ResponseEntity createAccount(
            @RequestBody BankAccountObject bankAccountObject
    ) {
        try {
            String accountNumber = bankAdapter.createAccount(
                    bankAccountObject.firstName,
                    bankAccountObject.lastName,
                    bankAccountObject.id,
                    bankAccountObject.balance
            );

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(accountNumber);

        } catch (BankException e) {

            switch (e.getCause().getMessage()) {

                case "Account already exists":
                    return ResponseEntity
                            .status(HttpStatus.CONFLICT)
                            .body("Bank: " + e.getMessage());
            }

            e.printStackTrace();

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Bank: " + e.getMessage());
        }
    }

    /**
     * deletes an account with the given accountnumber
     * @param accountNumber the account number of the account to be deleted
     * @return a Response with a Status code and possibly an error message.
     */
    @DeleteMapping("/account/{accountNumber}")
    public ResponseEntity deleteAccount(
            @PathVariable("accountNumber") String accountNumber
    ) {
        try {
            bankAdapter.deleteAccount(accountNumber);

            return ResponseEntity
                    .ok(null);

        } catch (BankException e) {
            e.printStackTrace();

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Bank: " + e.getMessage());
        }
    }

    /**
     * Gets an accounts balance based on the account number
     * @param accountNumber the account number to receive balance from
     * @return a response with a http status code and the balance of the account or an error message.
     */
    @GetMapping("/account/{accountNumber}/balance")
    public ResponseEntity getAccountBalance(
            @PathVariable("accountNumber") String accountNumber
    ) {
            try {
                BigDecimal balance = bankAdapter.getAccountBalance(accountNumber);

                return ResponseEntity
                        .ok(balance);

            } catch (BankException e) {
                e.printStackTrace();

                return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Bank: " + e.getMessage());
            }
        }

    /**
     * Gets a response with a list of transactions performed by the account
     * @param accountNumber the account number to get transactions from
     * @return a response wrapping a list of transactions or an error message
     */
    @GetMapping("/account/{accountNumber}/transactions")
    public ResponseEntity getAccountTransactions(
            @PathVariable("accountNumber") String accountNumber
    ) {
        try {
            List<BankTransactionInterface> transactions = bankAdapter.getAccountTransactions(accountNumber);

            return ResponseEntity
                    .ok(transactions);

        } catch (BankException e) {
            e.printStackTrace();

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Bank: " + e.getMessage());
        }
    }
}

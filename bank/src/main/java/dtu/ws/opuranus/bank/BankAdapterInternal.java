package dtu.ws.opuranus.bank;

import dtu.ws.fastmoney.*;
import dtu.ws.opuranus.interfaces.BankTransactionInterface;
import dtu.ws.opuranus.exceptions.BankException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Heino
 */
@Service
//@Primary
public class BankAdapterInternal
        extends AbstractBankAdapter {

    protected Bank bankInstance;

    public BankAdapterInternal() {
        bankInstance = new Bank();
    }

    protected dtu.ws.fastmoney.Bank getBankInstance() {
        return bankInstance;
    }

    /**
     * Makes a transfer between a customer and a merchant with a price and a description.
     * @param fromAccountNumber The account from which to transfer money
     * @param toAccountNumber The account the money are to be transferred to
     * @param amount The amount of money to transfer
     * @param description Information regarding the payment
     * @throws BankException if the transfer could not be performed.
     */
    @Override
    public void transfer(String fromAccountNumber, String toAccountNumber, BigDecimal amount, String description) throws BankException {
        try {
            getBankInstance().transferMoneyFromTo(
                    fromAccountNumber,
                    toAccountNumber,
                    amount,
                    description
            );

        } catch (BankServiceException e) {
            throw new BankException("Bank rejected request: " + e.getMessage(),e);
        }
    }

    /**
     * Creates an account in the bank
     * @param firstName of account holder
     * @param lastName of account holder
     * @param id of account
     * @param balance of account
     * @return the accountnumber of the newly created account
     * @throws BankException if the account could not be created
     */
    @Override
    public String createAccount(String firstName, String lastName, String id, BigDecimal balance) throws BankException {
        try {
            User user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setCprNumber(id);

            System.out.println(user);
            System.out.println(user.getFirstName());
            System.out.println(user.getLastName());
            System.out.println(user.getCprNumber());
            return getBankInstance()
                    .createAccountWithBalance(
                            user,
                            balance
                    );

        } catch (BankServiceException e) {
            throw new BankException("Bank rejected request: " + e.getMessage(), e);
        }
    }

    /**
     * Deletes an account in the bank
     * @param accountNumber the account number of the bank account to delete
     * @throws BankException if the account doesnt exist
     */
    @Override
    public void deleteAccount(String accountNumber) throws BankException {
        try {
            getBankInstance()
                    .retireAccount(
                            accountNumber
                    );

        } catch (BankServiceException e) {
            throw new BankException("Bank rejected request: " + e.getMessage(), e);
        }
    }

    /**
     * Gets the balance of an account
     * @param accountNumber the accountNumber of the bank account
     * @return the balance of the account
     * @throws BankException if the account does not exist
     */
    @Override
    public BigDecimal getAccountBalance(String accountNumber) throws BankException {
        try {
            return getBankInstance().getAccount(accountNumber).getBalance();

        } catch (BankServiceException e) {
            throw new BankException("Bank rejected request: " + e.getMessage(), e);
        }
    }

    /**
     * Gets a list of transactions performed by the account.
     * @param accountNumber the accountnumber of the account to find transactions for
     * @return the list of transactions performed by the account.
     * @throws BankException if the account does not exist
     */
    @Override
    public List<BankTransactionInterface> getAccountTransactions(String accountNumber) throws BankException {
        try {
            Account account = getBankInstance().getAccount(accountNumber);
            Transaction[] transactions = account.getTransactions();

            List<BankTransactionInterface> list = new LinkedList<>();
            for (Transaction transaction : transactions) {
                list.add(new BankTransaction(
                        transaction.getTime(),
                        transaction.getAmount(),
                        transaction.getBalance(),
                        transaction.getCreditor(),
                        transaction.getDebtor(),
                        transaction.getDescription()
                ));
            }
            return list;

        } catch (BankServiceException e) {
            throw new BankException("Bank rejected request: " + e.getMessage(), e);
        }
    }
/*
    @Override
    public String getAccountNumberByCprNumber(String cpr) throws BankException {
        try {
            Account account = getBankInstance().getAccountByCprNumber(cpr);
            return account.getId();

        } catch (BankServiceException e) {
            throw new BankException("Bank rejected request: " + e.getMessage(), e);
        }
    }

    @Override
    public String getAccountOwnerName(String accountNumber) throws BankException {
        try {
            Account account = bankInstance.getAccount(accountNumber);

            return account.getUser().getFirstName() + " " + account.getUser().getLastName();

        } catch (BankServiceException e) {
            throw new BankException("Bank rejected request: " + e.getMessage(), e);
        }
    }
*/
}

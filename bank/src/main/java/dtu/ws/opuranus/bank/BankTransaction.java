package dtu.ws.opuranus.bank;

import dtu.ws.opuranus.interfaces.BankTransactionInterface;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Heino
 */
public class BankTransaction
        implements BankTransactionInterface {

    private Date time;
    private BigDecimal amount;
    private BigDecimal balance;
    private String creditor;
    private String debtor;
    private String description;

    public BankTransaction(Date time, BigDecimal amount, BigDecimal balance, String creditor, String debtor, String description) {
        this.time = time;
        this.amount = amount;
        this.balance = balance;
        this.creditor = creditor;
        this.debtor = debtor;
        this.description = description;
    }

    @Override
    public String toString() {
        return "BankTransaction{" +
                "time=" + time +
                ", amount=" + amount +
                ", balance=" + balance +
                ", creditor='" + creditor + '\'' +
                ", debtor='" + debtor + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public Date getTime() {
        return time;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getCreditor() {
        return creditor;
    }

    public String getDebtor() {
        return debtor;
    }

    public String getDescription() {
        return description;
    }
}

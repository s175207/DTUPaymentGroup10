package dtu.ws.opuranus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Heino
 */
@SpringBootApplication(
        scanBasePackages = {
                "dtu.ws.opuranus"
        }
)
public class BankApplication {

    /**
     * Entry point for the BankApplication
     * @param args Command line arguments to run the application.
     */
    public static void main(String[] args) {
        SpringApplication.run(BankApplication.class, args);

    }
}

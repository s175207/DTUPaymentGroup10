package dtu.ws.opuranus.token;

import dtu.ws.opuranus.interfaces.TokenGeneratorInterface;
import dtu.ws.opuranus.interfaces.TokenManagerInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.TokenException;
import dtu.ws.opuranus.entity.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Anders
 * MerchantManager handles CRUD responsibilities for all Token objects inside the DTUPay webservice
 */
@Service
@Primary
public class TokenManager implements TokenManagerInterface {

    public static void main(String[] args) {
        //Create token
        TokenManager tokenmanager = new TokenManager();
        tokenmanager.generateAndSaveToken(1);
    }

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private TokenGeneratorInterface tokenGenerator;

    /**
     * Generates and saves a new Token to tokenRepository
     * @param customerId ID of the Customer being granted the Token
     * @return Returns the Token object
     */
    public Token generateAndSaveToken(int customerId) {
        Token token = tokenGenerator.generateToken(customerId);
        tokenRepository.save(token);
        return token;
    }

    /**
     * Get one token from tokenRepository
     * @param tokenId Integer ID of Token
     * @return Returns the token if it is found
     * @throws TokenException is thrown if the Token is unknown or invalidated
     */
    public Token getTokenById(Integer tokenId) throws TokenException {

        Optional<Token> token = tokenRepository.findById(tokenId);

        if (! token.isPresent()) {
            throw new TokenException("Unknown token");
        }

        if (! token.get().isValid()) {
            throw new TokenException("Token not valid");
        }

        return token.get();
    }


    /**
     * Request new Tokens for the Customer object
     * @param customer Customer object
     * @return A List of Tokens belonging to the Customer
     * @throws TokenException TokenException is thrown if Token services fails
     */
    public List<Token> requestTokens(Customer customer) throws TokenException {
        if (customer == null) {
            throw new NullPointerException("NULL");
        }

        return requestTokens(customer.getId());
    }

    /**
     * For a selected Customer object request new Tokens and return them as a list.
     * Partial method that checks for the maximal amount of tokens that the Customer can request.
     * @param customerId ID of Customer Object
     * @return List of Customer's Tokens
     * @throws TokenException
     */
    public List<Token> requestTokens(int customerId) throws TokenException {

        int numberOfTokens = getNumberOfTokens(customerId);

        int requestNumberOfTokens = numberOfTokens == 0 ? 5 : 6 - numberOfTokens;

        return requestTokens(customerId, requestNumberOfTokens);
    }

    /**
     * Support method to cucumber tests not meant for production service
     * @param customer Customer object
     * @param max Number of Tokens
     * @return List of Tokens
     * @throws TokenException
     */
    public List<Token> requestTokens(Customer customer, int max) throws TokenException {
        if (customer == null) {
            throw new NullPointerException("NULL");
        }

        return requestTokens(customer.getId(), max);
    }

    /**
     * Request new tokens for Customer. Checks for validity of request.
     * @param customerId ID of Customer object
     * @param max
     * @return
     * @throws TokenException
     */
    public List<Token> requestTokens(int customerId, int max) throws TokenException {

        if ((max < 1) || (max > 5)) {
            throw new IllegalArgumentException("You can request between 1 and 5 tokens");
        }

        int numberOfTokens = getNumberOfTokens(customerId);

        if (numberOfTokens > 1) {
            throw new TokenException("You can't request tokens when you have 2 or more tokens");
        }
        ArrayList<Token> tokenList = new ArrayList<>();
        for (int i = 0; i < max; i++) {
            tokenList.add(generateAndSaveToken(customerId));
        }
        return tokenList;
    }

    /**
     * Use and invalidate a token
     * @param token Token object to be used
     * @throws TokenException TokenException thrown if Token object is unknown
     */
    public void useToken(Token token) throws TokenException {

        if (token == null) {
            throw new NullPointerException("NULL");
        }

        if (! token.isValid()) {
            throw new TokenException("Token has been invalidated");
        }

        if (! tokenRepository.existsById(token.getId())) {
            throw new TokenException("Token is unknown");
        }

        token.invalidate();

        tokenRepository.save(token);
    }

    /**
     * Get the number of Tokens held by Customer object (by ID)
     * @param customerId Customer Id
     * @return number of Tokens
     */
    public int getNumberOfTokens(Integer customerId) {
        return tokenRepository.findByIdentityAndInvalidatedIsNullOrderByIssued(customerId).size();
    }

    /**
     * Get the number of Tokens held by Customer object
     * @param customer Customer object
     * @return number of Tokens
     */
    public int getNumberOfTokens(Customer customer) {
        return tokenRepository.findByIdentityAndInvalidatedIsNullOrderByIssued(customer.getId()).size();
    }

    /**
     * Get list of tokens in repository related to Customer ID
     * @param customerId int
     * @return return List of Tokens
     */
    public List<Token> getTokens(Integer customerId) {
        return tokenRepository.findByIdentityAndInvalidatedIsNullOrderByIssued(customerId);
    }

    /**
     * Get list of tokens in repository related to Customer
     * @param customer Customer object
     * @return return List of Tokens
     */
    public List<Token> getTokens(Customer customer) {
        return tokenRepository.findByIdentityAndInvalidatedIsNullOrderByIssued(customer.getId());
    }

    /**
     * Get list of tokens in repository related to Merchant
     * @param merchant Merchant object
     * @return return List of Tokens
     */
    public List<Token> getTokens(Merchant merchant) {
        return tokenRepository.findByIdentityAndInvalidatedIsNullOrderByIssued(merchant.getId());
    }

    /**
     * Delete Token from repository
     * @param token Token object to be deleted
     */
    @Override
    public void deleteToken(Token token) {
        tokenRepository.delete(token);
    }
}

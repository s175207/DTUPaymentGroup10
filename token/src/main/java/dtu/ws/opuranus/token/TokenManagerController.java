package dtu.ws.opuranus.token;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.TokenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Anders
 * TokenManagerController is REST controller between DTUPay and the TokenManager database.
 * The class handles database connections, database mapping, and object-relational mapping.
 */
@JsonIgnoreProperties
@RestController
@RequestMapping("/token")
public class TokenManagerController {

    @Autowired
    TokenManager tokenManager;

    @Autowired
    TokenGenerator tokenGenerator;
    private Object Token;

    /**
     * getToken retrieves a Token object from the database.
     * @param tokenId Integer ID for the requested Token
     * @return Returns a Token object wrapped in a ResponseEntity
     */
    @GetMapping("/getToken")
    public ResponseEntity getToken(
            @Param("tokenId") Integer tokenId
    ) {
        try {
            Token token = tokenManager.getTokenById(tokenId);

            return ResponseEntity
                    .ok(token);

        } catch (TokenException e) {
            return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(e.getMessage());
        }
    }

    /**
     * Returns the Tokens relating to a specific Customer
     * @param customerId Identity of Customer
     * @return A list of Tokens wrapped inside a ResponseEntity object
     */
    @GetMapping("/getTokens")
    public ResponseEntity<List<Token>> getTokens(
            @Param("customerId") Integer customerId
    ) {
//      try {
        List<Token> tokens = tokenManager.getTokens(customerId);

        return ResponseEntity
                .ok(tokens);

//      } catch (TokenException e) {
//          return ResponseEntity
//                  .status(HttpStatus.NOT_FOUND)
//                  .body(e.getMessage());
//      }
    }

    /**
     * Returns the number of Tokens that a specific Customer holds
     * @param customerId Identity of Customer
     * @return ResponseEntity object containing the integer amount of Tkoens held by Customer
     */
    @GetMapping("/numberOfTokens")
    public ResponseEntity<Integer> getNumberOfTokens(
            @Param("customerId") Integer customerId
    ) {
//      try {
            Integer tokens = tokenManager.getNumberOfTokens(customerId);

            return ResponseEntity
                     .ok(tokens);

//      } catch (TokenException e) {
//           return ResponseEntity
//                   .status(HttpStatus.NOT_FOUND)
//                   .body(e.getMessage());
//      }
    }


    /**
     * Handles Customer requests for new Tokens
     * @param customerId Customer ID
     * @param max The amount of Tokens requested by the Customer
     * @return ResponseEntity containing a string reply for the request.
     */
    @GetMapping("/requestTokens")
    public ResponseEntity requestTokens(
            @Param("customerId") Integer customerId,
            @Param("max") Integer max
    ) {
        try {
            List<Token> tokens = tokenManager.requestTokens(customerId, max);

            return ResponseEntity
                    .ok(tokens);

        } catch (TokenException e) {

            if (e.getMessage().equals("You can't request tokens when you have 2 or more tokens")) {
                return ResponseEntity
                        .status(HttpStatus.CONFLICT)
                        .body(e.getMessage());
            }

            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());

        } catch (IllegalArgumentException e) {

            if (e.getMessage().equals("You can request between 1 and 5 tokens")) {
                return ResponseEntity
                        .status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .body(e.getMessage());
            }

            throw e;
        }
    }

    /**
     * Request a Token from server
     * @param token Token object
     * @return ResponseEntity containing server reply
     */
    @GetMapping("/useToken")
    public ResponseEntity request(
            @Param("token") Token token
    ) {
        try {
            tokenManager.useToken(token);

            return ResponseEntity
                    .ok(null);

        } catch (TokenException e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }

    /**
     * Delete a Token from server
     * @param token Token object
     * @return ResponseEntity containing server reply
     */
    @GetMapping("/deleteToken")
    public ResponseEntity deleteToken(
            @Param("token") Token token
    ) {
        tokenManager.deleteToken(token);

        return ResponseEntity
                .ok(null);

    }


   /* @GetMapping("/generateToken")
    public ResponseEntity generateAndSaveToken(
        @Param("customerId") Integer customerId){

        tokenGenerator.generateToken(customerId);

        return ResponseEntity.ok(null);
    } */

    /**
     * Generate a new Token
     * @param id String id of new Token
     * @return ResponseEntity "ok"
     */
    @PostMapping("/generateToken")
    public ResponseEntity transfer(
            @RequestBody Integer id
    ){
        tokenGenerator.generateToken(id);


        return ResponseEntity.ok(null);
    }



}

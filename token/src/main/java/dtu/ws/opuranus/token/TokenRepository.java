package dtu.ws.opuranus.token;

import dtu.ws.opuranus.entity.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author Anders
 * TokenRepository is the class used by DTUPay to handle tokens in the database.
 * TokenRepository extends CrudRepository giving the class Create, Read, Update, Delete functionalities.
 */
@Repository
public interface TokenRepository extends CrudRepository<Token, Integer> {

    /**
     * Retrieve one Token with [ID] from database
     * @param id integer identifying a specific Token object
     * @return returns (if found) the Token object from database
     */
    @Override
    Optional<Token> findById(@Param("id") Integer id);

    /**
     * Returns the number of Tokens relating to a specific Customer
     * @param identity Identity of Customer
     * @return Number of Tokens held by the Customer
     */
    Integer countByIdentity(@Param("identity") Integer identity);

    /**
     * Returns all the Tokens relating to a specific Customer
     * @param identity Identity of Customer
     * @return The Set of Tokens held by the Customer
     */
    Set<Token> findByIdentity(@Param("itentity") Integer identity);

    /**
     * Returns the valid Tokens relating to a specific Customer
     * @param identity Identity of Customer
     * @return Number of valid Tokens held by the Customer. List is sorted by time of issueing.
     */
    List<Token> findByIdentityAndInvalidatedIsNullOrderByIssued(@Param("identity") Integer identity);

    // Look at: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods
//  List<Token> findByCustomerIdInvalidatedIsNullByOrderByIssuedAsc(@Param("customerId") Integer customerId);
}

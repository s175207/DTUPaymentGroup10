package dtu.ws.opuranus.token;

import dtu.ws.opuranus.entity.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Anders
 * TokenGeneratorController acts as a controller of the TokenGenerator.
 * The class handles database connections, database mapping, and object-relational mapping.
 */
@RestController
@RequestMapping("/tokenGenerator")
public class TokenGeneratorController {

    @Autowired
    TokenGenerator tokenGenerator;

    /**
     * Controller generates new Token with an identifier matching a database record
     * @param identity Identifier integer
     * @return A token wrapped in a ResponseEntity
     */
    @GetMapping("/generate")
    public ResponseEntity getToken(
            @Param("identity") Integer identity
    ) {
//      try {
            Token token = tokenGenerator.generateToken(identity);

            return ResponseEntity
                    .ok(token);

//        } catch (TokenException e) {
//            return ResponseEntity
//                .status(HttpStatus.NOT_FOUND)
//                .body(e.getMessage());
//        }
    }
}

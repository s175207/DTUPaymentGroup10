package dtu.ws.opuranus.token;

import dtu.ws.opuranus.interfaces.TokenGeneratorInterface;
import dtu.ws.opuranus.entity.Token;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @author Anders
 * TokenGenerator is responsible for generating all new Tokens.
 */
@Component
@Primary
public class TokenGenerator implements TokenGeneratorInterface {

    private static Random random = new Random();

    /**
     * This method generates new Tokens for DTUPay. It instantiates a new Token object and sets the Token ID.
     * @param identity Identity of the generated Token
     * @return  Returns the Token.
     */
    public Token generateToken(int identity) {
        Token token = new Token();
        token.setId(random.nextInt());
        token.setIdentity(identity);
        return token;
    }
}

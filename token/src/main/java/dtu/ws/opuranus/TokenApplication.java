package dtu.ws.opuranus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Anders
 */

@SpringBootApplication(
        scanBasePackages = {
                "dtu.ws.opuranus"
        }
)
public class TokenApplication {
    public static void main(String[] args) {
        SpringApplication.run(TokenApplication.class, args);

    }
}

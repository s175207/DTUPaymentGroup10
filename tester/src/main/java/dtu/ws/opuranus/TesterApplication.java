package dtu.ws.opuranus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Mathias
 */
@SpringBootApplication(
        scanBasePackages = {
                "dtu.ws.opuranus"
        }
)
public class TesterApplication {
    public static void main(String[] args) {
        SpringApplication.run(TesterApplication.class, args);
    }
}

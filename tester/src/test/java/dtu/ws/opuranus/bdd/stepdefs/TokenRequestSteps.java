package dtu.ws.opuranus.bdd.stepdefs;

import dtu.ws.opuranus.interfaces.CustomerManagerInterface;
import dtu.ws.opuranus.interfaces.TokenManagerInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.BankException;
import dtu.ws.opuranus.exceptions.CustomerException;
import dtu.ws.opuranus.exceptions.TokenException;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Anders
 */

public class TokenRequestSteps {

    @Autowired
    private CustomerManagerInterface customerManager;

    @Autowired
    private TokenManagerInterface tokenManager;

    int requested;
    private Customer customer;
    private Merchant merchant;
    List<Token> list = new ArrayList<>();

    private Exception exception;

    private BigDecimal givenAmount;
    private BigDecimal givenCustomerBalance;
    private BigDecimal givenMerchantBalance;


    @After
    public void cleanUp() throws BankException, CustomerException {
// Todo: Should NOT use repositories directly...
        if(customerManager !=null && customer!=null) {
            customerManager.deleteCustomer(customer);
          /*  for (Token t : tokenRepository.findByIdentity(customer.getId())) {
                tokenRepository.delete(t);
            }*/
            exception = null;
            requested = 0;
        }
    }


    //    @Given("the customer has at least {int} valid token")
//    public void theCustomerHasAtLeastValidToken(int tokens) {
    @Given("I have {int} tokens")
    public void IHaveIntTokens(int tokens) throws CustomerException, TokenException, BankException {
        customer = customerManager.createCustomer("123", "123");
        if(tokens !=0) {
            tokenManager.requestTokens(customer, tokens);
        }
// TODO:    t.setCustomerId(customer);
        System.out.println("aaa" + tokenManager.getTokens(customer).size());

// TODO: System.out.println("IHaveIntTokens tokens " + tokens + ", customer.tokens.len "+customer.tokens.size());
    };

    @When("I request to get {int} new tokens")

    public void IRequestToGetIntNewTokens(int tokens) {
        try
        {
            list = tokenManager.requestTokens(customer.getId(),tokens);
        } catch(IllegalArgumentException | TokenException | BankException e)
        {
            exception = e;
        }
        requested = tokens;

    };
    @Then("I should get {string}")
    public void IShouldGeString(String string) throws TokenException {

        if (string.equals("")) { // if the transfer succeeded the error is empty.
            assertNull(exception);
            assertEquals(requested, list.size());
        } else if(exception != null) {
            assertEquals(string, exception.getMessage());
            assertEquals(0, list.size());
        }
    };
    @Then("I should have {int} tokens")
    public void IShouldHaveIntTokens(int int1) {
        assertEquals(int1, tokenManager.getNumberOfTokens(customer.getId()));
    };
}

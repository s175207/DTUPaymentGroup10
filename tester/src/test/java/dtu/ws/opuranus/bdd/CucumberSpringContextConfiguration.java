package dtu.ws.opuranus.bdd;

import cucumber.api.java.Before;
import dtu.ws.opuranus.TesterApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;

/**
 * @author Heino
 */
@SpringBootTest(
        webEnvironment = WebEnvironment.DEFINED_PORT
)
@ContextConfiguration(
        classes = {
                TesterApplication.class,
        },
        loader = SpringBootContextLoader.class
)
public class CucumberSpringContextConfiguration {

  private static final Logger LOG = LoggerFactory.getLogger(CucumberSpringContextConfiguration.class);

  /**
   * Need this method so the cucumber will recognize this class as glue and load spring context configuration
   */
  @Before
  public void setUp() {
    LOG.info("-------------- Spring Context Initialized For Executing Cucumber Tests --------------");
  }
}


package dtu.ws.opuranus.bdd.stepdefs;

import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.TokenException;
import dtu.ws.opuranus.interfaces.BankAdapterInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.exceptions.CustomerException;
import dtu.ws.opuranus.interfaces.CustomerManagerInterface;
import dtu.ws.opuranus.interfaces.MerchantManagerInterface;
import dtu.ws.opuranus.interfaces.TokenManagerInterface;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Rasmus
 */
public class CleanupHooks {

    @Autowired
    private CustomerManagerInterface customerManager;

    @Autowired
    private MerchantManagerInterface merchantManager;

    @Autowired
    private TokenManagerInterface tokenManager;

    @Autowired
    private BankAdapterInterface bankAdapter;

    static public final Set<String> accounts = new HashSet<String>();

    @Before( value = "@Cleanup:Bank", order = 1 )
    @After(  value = "@Cleanup:Bank", order = 1000 )
    public void cleanupBankAccounts() {
        for (String account : accounts) {
            deleteBankAccount(account);

        }
    }

    @Before( value = "@Cleanup:Users", order = 1 )
    @After(  value = "@Cleanup:Users", order = 1000 )
    public void cleanupUsers() throws CustomerException {

        for (Customer customer : customerManager.getCustomerList()) {

            deleteBankAccount(customer.getAccountNumber());

            for (Token token : tokenManager.getTokens(customer)) {
                try {
                    tokenManager.useToken(token);
                } catch (TokenException e) {
                }
            }

            customerManager.deleteCustomer(customer);
        }

        for (Merchant merchant : merchantManager.getMerchantList()) {
            deleteBankAccount(merchant.getAccountNumber());

            merchantManager.deleteMerchant(merchant);
        }
    }

    private void deleteBankAccount(String accountNumber) {
        try {
            bankAdapter.deleteAccount(accountNumber);
        } catch (Exception e) {
        }
    }
}

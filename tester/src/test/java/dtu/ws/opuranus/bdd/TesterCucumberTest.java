package dtu.ws.opuranus.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Mathias
 * To run cucumber test
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {
                "not @ignore"
        },
        strict = true,
        snippets = CucumberOptions.SnippetType.UNDERSCORE,
        features = {
                "classpath:features"
        },
        glue = {
        },
        plugin = {
                "pretty",
                "html:**/target/cucumber-report.html",
                "json:**/target/cucumber-report.json",
                "junit:**/target/cucumber-report.xml"
        }
)
public class TesterCucumberTest {
}

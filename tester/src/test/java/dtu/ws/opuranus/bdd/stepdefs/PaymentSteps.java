package dtu.ws.opuranus.bdd.stepdefs;

import dtu.ws.opuranus.interfaces.*;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.BankException;
import dtu.ws.opuranus.exceptions.CustomerException;
import dtu.ws.opuranus.exceptions.MerchantException;
import dtu.ws.opuranus.exceptions.TokenException;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Mathias
 */
public class PaymentSteps {

    @Autowired
    private BankAdapterInterface bankAdapter;

    @Autowired
    private CustomerManagerInterface customerManager;

    @Autowired
    private MerchantManagerInterface merchantManager;

    @Autowired
    private TokenManagerInterface tokenManager;

    @Autowired
    private PaymentGatewayInterface paymentGateway;

    private Customer customer;
    private Merchant merchant;

    private List<Token> customerTokens;
    private Token scannedToken;

    private Integer transactionToken;

    private Exception exception;

    @Before("@Feature:Payment")
    public void before() {
        customer = null;
        merchant = null;
        customerTokens = null;
        scannedToken = null;
        transactionToken = null;
        exception = null;
    }

    @After("@Feature:Payment")
    public void after() throws CustomerException {
        try {
            bankAdapter.deleteAccount(customer.getAccountNumber());
        } catch (BankException e) {
        }
        customerManager.deleteCustomer(customer);

        try {
            bankAdapter.deleteAccount(merchant.getAccountNumber());
        } catch (BankException e) {
        }
        merchantManager.deleteMerchant(merchant);
    }

    @Given("(a/the) customer {string} with cpr {string} is registered with DTU Pay")
    public void theCustomerWithCprIsRegisteredWithDTUPay(String name, String cpr) throws CustomerException {
        customer = customerManager.createCustomer(name, cpr);
    }

    @And("(a/the) customer has an account with the bank with a balance of {int} kr")
    public void theCustomerHasAnAccountWithTheBankWithABalanceOfKr(int balance) throws BankException, CustomerException {
        customer.setAccountNumber(
            bankAdapter.createAccount(customer, BigDecimal.valueOf(balance))
        );
        customerManager.saveCustomer(customer);
    }

    @And("(a/the) customer has requested {int} token(s)")
    public void theCustomerHasRequestedToken(int numberOfTokens) throws TokenException, BankException {
        customerTokens = tokenManager.requestTokens(customer, numberOfTokens);
    }

    @And("(a/the) customer has at least {int} token(s) left")
    public void theCustomerHasAtLeastTokenLeft(int tokens) {
        assertTrue(tokenManager.getTokens(customer).size() >= tokens);
    }

    @And("(a/the) merchant {string} with cvr {string} is registered with DTU pay")
    public void theMerchantWithCvrIsRegisteredWithDTUPay(String name, String cvr) throws BankException {
        merchant = merchantManager.createMerchant(name, cvr);
    }

    @And("(a/the) merchant has an account with the bank with a balance of {int} kr")
    public void theMerchantHasAnAccountWithTheBankWithABalanceOfKr(int balance) throws BankException, MerchantException {
        String x;
        merchant.setAccountNumber(
                bankAdapter.createAccount(merchant, BigDecimal.valueOf(balance))
        );
        merchantManager.saveMerchant(merchant);
    }

    @Given("(a/the) customer has at least {int} valid token")
    public void theCustomerHasAtLeastValidToken(int tokens) {
        assertTrue(tokenManager.getTokens(customer).size() >= tokens);
    }

    @When("(a/the) merchant has scanned the customers token")
    public void theMerchantMerchantScansTheCustomersToken() {
        scannedToken = customerTokens.get(0);
    }

    @And("the token is valid")
    public void theTokenIsValid() {
        assertTrue(scannedToken.isValid());
    }

    @When("the merchant requests {int} kr from the customer using the token")
    public void theMerchantRequestsKrFromTheCustomerUsingTheToken(int amount) {
        try {
            Customer customer = customerManager.getCustomerByIdentity(scannedToken.getIdentity());

            transactionToken = paymentGateway.doTransaction(
                    merchant,
                    scannedToken.getId(),
                    BigDecimal.valueOf(amount),
                    "description"
            );

        } catch (Exception e) {
            exception = e;
            e.printStackTrace();
        }
    }

    @Then("the transaction is successful")
    public void transactionIsSuccessful() {
        assertNotNull(transactionToken);
    }

    @And("the balance of the customer's bank account is {int} kr")
    public void theBalanceOfTheCustomerSBankAccountIsKr(int balance) throws BankException {
        assertEquals(BigDecimal.valueOf(balance), bankAdapter.getAccountBalance(customer.getAccountNumber()));
    }

    @And("the balance of the merchant's bank account is {int} kr")
    public void theBalanceOfTheMerchantSBankAccountIsKr(int balance) throws BankException {
        assertEquals(BigDecimal.valueOf(balance), bankAdapter.getAccountBalance(merchant.getAccountNumber()));
    }

    @And("the customer has {int} valid token(s)")
    public void theCustomerHasValidToken(int tokens) {
        assertEquals(1, tokenManager.getNumberOfTokens(customer));
    }
}

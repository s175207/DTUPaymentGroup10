package dtu.ws.opuranus.bdd.stepdefs;

import dtu.ws.opuranus.interfaces.CustomerManagerInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.exceptions.CustomerException;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import static org.junit.Assert.*;

/**
 * @author Daniel
 */
public class CustomerSteps {

    @Autowired
    private CustomerManagerInterface customerManager;

    private Customer customer;
    private Customer existingCustomer;

    private String givenName;
    private String givenCpr;

    private Exception resultException;

    {{
        CleanupHooks.accounts.add("214193543");
    }};

    @Before("@Feature:Customer")
    public void before() throws CustomerException {
        customer = null;
        resultException = null;
        givenName = "Noe Body";
        givenCpr = "123456-7890";

        existingCustomer = customerManager.createCustomer(
                "Existing Customer",
                "098765-4321"
        );
    }

    @After("@Feature:Customer")
    public void after() {
        try{
            customerManager.deleteCustomer(existingCustomer);

            if (customer != null) {
                customerManager.deleteCustomer(customer);
            }
        }catch (CustomerException e){
            e.getMessage();
        }

    }

    @Given("(a/the) customer's name is {string}")
    public void customerSNameIs(String name) {
        givenName = name;
    }

    @Given("(a/the) customer's cpr is {string}")
    public void theCustomerSCprIs(String cpr) {
        givenCpr = cpr;
    }

    @Given("the customer does not exist")
    public void theCustomerDoesNotExist() {
        System.out.println(customerManager.hasCustomerByCpr(givenCpr));
        assertFalse(customerManager.hasCustomerByCpr(givenCpr));
        assertFalse(customerManager.hasCustomerByName(givenName));
    }

    @But("the customer (already )exists")
    public void theCustomerAlreadyExists() throws Exception {

        if (givenName == null && givenCpr == null) {
            throw new Exception("Neither name nor cpr is given.");
        }

        if (givenName != null) {
            customer = customerManager.getCustomerByName(givenName);
            if (givenCpr != null) {
                assertEquals(givenCpr, customer.getCpr());
            }
        }
        if (givenCpr != null) {
            customer = customerManager.getCustomerByCpr(givenCpr);
            if (givenName != null) {
                assertEquals(givenName, customer.getName());
            }
        }

        assertNotNull(customer);
    }

    @Given("the administrator tries to create (a/the) customer")
    public void theAdministratorCreatesANewCustomerNamedWithCpr() {
        try {
            customer = customerManager.createCustomer(givenName, givenCpr);

        } catch (Exception e) {
            // This was expected...
            resultException = e;
        }
    }

    @Given("the administrator tries to delete (a/the) customer")
    public void theAdministratorTriesToDeleteACustomerNamedWithCpr() {
        try {
            customerManager.deleteCustomer(customer);

        } catch (InvalidDataAccessApiUsageException e) {
            resultException = e;

        } catch (Exception e) {
            resultException = e;
        }
    }

    @Then("the customer is (successfully )created")
    public void theCustomerIsCreated() throws Exception {
        assertNull(resultException);
    }

    @Then("creation of the customer fails")
    public void theCustomerIsNotCreated() {
        assertNotNull(resultException);
    }

    @Then("the customer is (successfully )deleted")
    public void theCustomerIsDeleted() {
        assertNull(resultException);
    }

    @Then("deletion of the customer fails")
    public void theCustomerIsNotRemoved() {
        assertNotNull(resultException);
    }
}

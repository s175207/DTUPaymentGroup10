package dtu.ws.opuranus.bdd.stepdefs;

import dtu.ws.opuranus.interfaces.*;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.entity.Transaction;
import dtu.ws.opuranus.exceptions.*;
import dtu.ws.opuranus.report.ReportManager;
import dtu.ws.opuranus.report.TransactionRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Rasmus
 */

public class ReportManagerSteps {
/*
    private Customer customerA;
    private Customer customerB;
    private Merchant merchantA;
    private String reportmessageA;
    private String reportmessageB;

    @Autowired
    private BankAdapterInterface bankAdapter;

    @Autowired
    MerchantManagerInterface merchantManager;

    @Autowired
    CustomerManagerInterface customerManager;

    @Autowired
    private TokenManagerInterface tokenManager;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private PaymentGatewayInterface paymentGateway;

    @Autowired
    private ReportManager reportManager;

    @Autowired
    private HelperBankAccountCreator helper;

//    @Before
    public void setUp() throws Exception {

        merchantA = merchantManager.createMerchant("Dont Fail", "171w7sa171717");
        merchantA.setAccountNumber(
            bankAdapter.createAccount(merchantA, BigDecimal.valueOf(2000))
        );
        merchantManager.saveMerchant(merchantA);

    }

    @Given("I am a customer of DTUPay")
    public void customerOfDTUPay() throws BankException, CustomerException {
        customerA = helper.createCustomerAndBankAccount("Rich Fawkes", "03030270101", BigDecimal.valueOf(2000));
        customerManager.saveCustomer(customerA);
        customerB = helper.createCustomerAndBankAccount("Guy Richie", "03a0302701dg01", BigDecimal.valueOf(2000));
        customerManager.saveCustomer(customerB);
    }

    @Given("I am a merchant registered with DTUPay")
    public void merchantOfDTUPay() throws BankException, CustomerException {
        customerA = helper.createCustomerAndBankAccount("Rich Fawkes", "03030270101", BigDecimal.valueOf(2000));
        customerManager.saveCustomer(customerA);
        customerB = helper.createCustomerAndBankAccount("Guy Richie", "03a0302701dg01", BigDecimal.valueOf(2000));
        customerManager.saveCustomer(customerB);
    }

    @Given("I have used DTUPay two times within the last month")
    public void customerHasDoneTransactionsWithinTheLastMonth() throws CustomerException, MerchantException, TokenException, BankException {
        ArrayList<Token> tokensCustomerA = (ArrayList<Token>) tokenManager.requestTokens(customerA, 3);

        //generate two payments within the month, and one older
        for(int i = 0; i<3; i++) {
            paymentGateway.doTransaction(
                    merchantA,
                    tokensCustomerA.get(i).getId(),
                    new BigDecimal((i+1.75)), // junk number
                    "Købt " + i + " lunkne mælkesnitter");
        }
        Transaction oldTransaction = transactionRepository.findById(tokensCustomerA.get(0).getId()).get();
        oldTransaction.setTransactionTime(new Date(1579490112738l - (31l * 24l * 60l * 60l * 1000l))); // moving transaction time by 31 days
        transactionRepository.save(oldTransaction);

        oldTransaction = transactionRepository.findById(tokensCustomerA.get(1).getId()).get();
        oldTransaction.setTransactionTime(new Date(1579490112738l - (10l * 24l * 60l * 60l * 1000l)));
        transactionRepository.save(oldTransaction);

        oldTransaction = transactionRepository.findById(tokensCustomerA.get(2).getId()).get();
        oldTransaction.setTransactionTime(new Date(1579490112738l));
        transactionRepository.save(oldTransaction);

    }

    @Given("I have done no transactions using DTUPay within the last month")
    public void customerHasDoneNoTransactionsUsingDTUPayWithinTheLastMonth() throws CustomerException, MerchantException, TokenException, BankException {
        ArrayList<Token> tokensForCustomerWithNoTransactionsWithinMonth = (ArrayList<Token>) tokenManager.requestTokens(customerA, 1);

        //generate a payment older than 30 days to test that no payments is returned
        paymentGateway.doTransaction(
                merchantA,
                tokensForCustomerWithNoTransactionsWithinMonth.get(0).getId(),
                new BigDecimal(12.75),
                "24 lunkne mælkesnitter");


        Transaction oldTransaction = transactionRepository.findById(tokensForCustomerWithNoTransactionsWithinMonth.get(0).getId()).get();
        oldTransaction.setTransactionTime(new Date(oldTransaction.getTransactionTime().getTime() - (31l * 24l * 60l * 60l * 1000l))); // moving transaction time by 31 days
        transactionRepository.save(oldTransaction);

    }


    @Given("I have had three costumers do transactions on DTUPay within the last month")
    public void merchantHadThreeCustomersDoTransactionsOnDTUPayWithinTheLastMonth() {
        // Write code here that turns the phrase above into concrete actions
        //delete??
        throw new cucumber.api.PendingException();
    }


    @Given("I have had no customers do transactions on DTUPay within the last month")
    public void testNoTransactionsOnDTUPayWithinTheLastMonth() {
    }

    @When("I request to get my monthly report")
    public void customerRequestForMonthlyReport() {
        reportmessageA = reportManager.transactionReport(customerA);
        reportmessageB = reportManager.transactionReport(customerB);
    }
    @When("I request to get my monthly merchant report")
    public void customerRequestForMonthlyMerchantReport() {
        reportmessageA = reportManager.transactionReport(merchantA);
    }

    ;

    @Then("I should get a digest of both my transactions")
    public void testGetDigestofBothTransactions(List<String> dataTable)  {
        String s ="";
        System.out.println(reportmessageA);
        for (int i=0; i< dataTable.size(); i+=6){
            s = dataTable.get(i+0);
            System.out.println(s);
            assertTrue(reportmessageA.contains(s));
            //skipping random token id.
            s = customerA.getId()+", "+merchantA.getId() +", " + dataTable.get(i+4)+", "+dataTable.get(i+5)+("\n");
            System.out.println(s);
            //assertTrue(reportmessageA.contains(s));
        }
    }

    ;

    @Then("I should get a digest of all transactions I've done in the last month")
    public void testGetADigestOfAllTransactionsDoneInTheLastMonth(List<String> dataTable) {
        String s = "";
        System.out.println("reportmessageA " +reportmessageA);
        for (int i = 0; i < dataTable.size(); i += 6) {
            s = dataTable.get(i + 0);
            System.out.println("timestamp: "+s);
            assertTrue(reportmessageA.contains(s));
            //skipping random token id.
            s = "0" + ", "+merchantA.getId() + ", " + dataTable.get(i + 4) + ", " + dataTable.get(i + 5) + ("\n");
            System.out.println("remaining: "+s);
            //assertTrue(reportmessageA.contains(s));
        }
    }
    ;

    @Then("I should get a {string} message")
    public void noTransactionsFoundMerchantAndCustomer(String noTransactionsFound) {

        assertEquals(noTransactionsFound,reportmessageB);
    }

//    @After
    public void tearDown() throws BankException, CustomerException {
        // Removing all tokens used in test
        if (customerA != null)
        /*    for (Token t : tokenRepository.findByIdentity(customerA.getId())) {
                System.out.println(t);
                tokenRepository.delete(t);
            }*/
      //  if (customerB != null)
            /*for (Token t : tokenRepository.findByIdentity(customerB.getId())) {
                tokenRepository.delete(t);
            }*/
/*for (Token t : tokenRepository.findByCustomerId(customerWithNoTransactionsWithinMonth.getId())) {
            tokenRepository.delete(t);
        }
*/
   /*     if (merchantA != null)
            bankAdapter.deleteAccount(merchantA.getAccountNumber());
        if (customerA != null)
            bankAdapter.deleteAccount(customerA.getAccountNumber());
        if (customerB != null)
            bankAdapter.deleteAccount(customerB.getAccountNumber());

        // Removing all customers and merchants used for test
        if (customerA != null)
            customerManager.deleteCustomer(customerA);
        if (customerB != null)
            customerManager.deleteCustomer(customerB);
        if (merchantA != null)
            merchantManager.deleteMerchant(merchantA);
        paymentGateway = null;
    }
*/
}

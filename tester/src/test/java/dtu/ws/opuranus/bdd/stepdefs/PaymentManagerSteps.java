package dtu.ws.opuranus.bdd.stepdefs;

import dtu.ws.opuranus.interfaces.*;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.exceptions.*;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mathias
 */

public class PaymentManagerSteps {
    @Autowired
    private BankAdapterInterface bankAdapter;


    @Autowired
    private TokenManagerInterface tokenManager;


    @Autowired
    private PaymentGatewayInterface paymentGateway;

    @Autowired
    private CustomerManagerInterface customerManager;

    @Autowired
    private MerchantManagerInterface merchantManager;

    @Autowired
    private HelperBankAccountCreator helper;

    BankException bankException;
    Customer customer;
    Merchant merchant;
    List<Token> tokens = new ArrayList<>();
    List<String> accounts = new ArrayList<>();
    String errormessage = "";
    int transaactionId;
    BigDecimal customerStartBalance;
    BigDecimal merchantStartBalance;
    BigDecimal customerBalanceBeforeRefund;
    BigDecimal merchantBalanceBeforeRefund;
    Customer thirdPart;

    @After
    public void preparePaymentManager() throws BankException {
        errormessage = "";
        tokens = new ArrayList<>();
        customerStartBalance = null;
        merchantStartBalance = null;
        customerBalanceBeforeRefund = null;
        merchantBalanceBeforeRefund = null;
    }

    @After
    public void cleanUpPaymentManager() throws BankException, CustomerException {
       if (customer != null) {
           bankAdapter.deleteAccount(customer.getAccountNumber());
           customerManager.deleteCustomer(customer);
       }
       if (thirdPart != null){
           bankAdapter.deleteAccount(thirdPart.getAccountNumber());
           customerManager.deleteCustomer(thirdPart);
       }
       if (merchant != null) {
           bankAdapter.deleteAccount(merchant.getAccountNumber());
           merchantManager.deleteMerchant(merchant);
       }
    }

    @Given("the customer has an account with DTU pay with a balance of {} kr.")
    public void theCustomerHasAnAccountWithDTUPayWithABalanceOfCustomerBalanceKr(BigDecimal customerBalance) throws BankException, TokenException, CustomerException {
        customer = customerManager.createCustomer("nick jayyy", "1010109-0594");
        customer.setAccountNumber(bankAdapter.createAccount(customer , customerBalance));
        customerManager.saveCustomer(customer);
        Assert.assertEquals("nick jayyy", customer.getName());
        Assert.assertEquals(customerBalance, bankAdapter.getAccountBalance(customer.getAccountNumber()));
        tokens = tokenManager.requestTokens(customer, 1);
        customerStartBalance = customerBalance;
    }

    @And("the customer has {int} tokens")
    public void theCustomerHasNumOfTokensTokens(int numOfTokens){
       Assert.assertEquals(numOfTokens, tokenManager.getNumberOfTokens(customer));
    }

    @And("the customer pay the merchant {int} kr and with the description {string}.")
    public void theCustomerPayTheMerchantKrWithTheDescription(int amount, String description) throws CustomerException, MerchantException, TokenException, BankException {
        try {
            transaactionId = paymentGateway.doTransaction(merchant, tokens.get(0).getId(), BigDecimal.valueOf(amount), description );
        } catch (BankException e) {
            errormessage = e.getMessage();
        }
    }

    @And("a merchant has an account with DTU pay with a balance of {} kr.")
    public void aMerchantHasAnAccountWithDTUPayWithABalanceOfMerchantBalanceKr(BigDecimal merchantBalance) throws BankException, MerchantException {
        merchant = merchantManager.createMerchant("Anders Mogensen","212121-9108");
        merchant.setAccountNumber(
                bankAdapter.createAccount(merchant, merchantBalance)
        );
        merchantManager.saveMerchant(merchant);

        merchantStartBalance = merchantBalance;
    }

    @When("the customer pay the merchant {int} kr with the description {string}.")
    public void theCustomerPayTheMerchantPaymentKrWithTheDescriptionPaymentDescription(int payment, String paymentDescription) throws CustomerException, MerchantException, TokenException {
        try {
            System.out.println("Merchant: " + merchant);
            System.out.println("Token: " + tokens.get(0).getId());
            System.out.println("Payment: " + BigDecimal.valueOf(payment));
            System.out.println("PaymentDescription: " + paymentDescription);
            transaactionId = paymentGateway.doTransaction(merchant, tokens.get(0).getId(), BigDecimal.valueOf(payment), paymentDescription);
        } catch (BankException e) {
            errormessage = e.getMessage();
        }
    }

    @Then("the system responds with {string}")
    public void theSystemRespondsWithResponse(String response) {
        System.out.println("får: " + errormessage+ " forventer: "+response);
        Assert.assertTrue(errormessage.contains(response));
    }

    @And("the merchant has an account with DTU pay with a balance of {int} kr.")
    public void theMerchantHasAnAccountWithDTUPayWithABalanceOfKr(int balance) throws BankException, MerchantException {
        merchantStartBalance = BigDecimal.valueOf(balance);
        merchant = merchantManager.createMerchant("hey yo","823057-3884");
        merchant.setAccountNumber(
            bankAdapter.createAccount(merchant, merchantStartBalance)
        );
        merchantManager.saveMerchant(merchant);
    }

    @When("the customer makes a refund on the purchase")
    public void theCustomerMakesARefundOnThePurchase() throws CustomerException, MerchantException {
        try {
            customerBalanceBeforeRefund = bankAdapter.getAccountBalance(customer.getAccountNumber());
            merchantBalanceBeforeRefund = bankAdapter.getAccountBalance(merchant.getAccountNumber());
            errormessage = paymentGateway.doRefund(transaactionId, "Refund");
        } catch (BankException e) {
             errormessage = e.getMessage();
        } catch (TransactionException transactionException) {
            errormessage = transactionException.getMessage();
        }
    }

    @And("the money is transferred from the merchant to the customer")
    public void theMoneyIsTransferredFromTheMerchantToTheCustomer() throws BankException {
        Assert.assertEquals(bankAdapter.getAccountBalance(customer.getAccountNumber()).intValue(),customerStartBalance.intValue());
        Assert.assertEquals(bankAdapter.getAccountBalance(merchant.getAccountNumber()).intValue(),merchantStartBalance.intValue());
    }

    @And("the merchant spends some of the money")
    public void theMerchantSpendsSomeOfTheMoney() throws BankException, CustomerException {
        thirdPart = customerManager.createCustomer("Peter Larsen" , "8923740-3847");
        thirdPart.setAccountNumber(bankAdapter.createAccount(thirdPart , BigDecimal.valueOf(50)));
        customerManager.saveCustomer(thirdPart);

        bankAdapter.transfer(merchant.getAccountNumber(), thirdPart.getAccountNumber(), BigDecimal.valueOf(50), "Payment");
    }

    @And("the refund is rejected and no money i transferred")
    public void theRefundIsRejectedAndNoMoneyITransferred() throws BankException {
       if(errormessage.equals("") || errormessage.contains("Transaction already refunded")){
           Assert.assertEquals(bankAdapter.getAccountBalance(customer.getAccountNumber()).intValue(),customerStartBalance.intValue());
           Assert.assertEquals(bankAdapter.getAccountBalance(merchant.getAccountNumber()).intValue(),merchantStartBalance.intValue());
       }else {
           Assert.assertEquals(bankAdapter.getAccountBalance(customer.getAccountNumber()).intValue(),customerBalanceBeforeRefund.intValue());
           Assert.assertEquals(bankAdapter.getAccountBalance(merchant.getAccountNumber()).intValue(),merchantBalanceBeforeRefund.intValue());
       }
    }

    @When("the customer makes a refund on a non existing purchase")
    public void theCustomerMakesARefundOnANonExistingPurchase() throws CustomerException, MerchantException {
        try {
            customerBalanceBeforeRefund = bankAdapter.getAccountBalance(customer.getAccountNumber());
            merchantBalanceBeforeRefund = bankAdapter.getAccountBalance(merchant.getAccountNumber());
            errormessage = paymentGateway.doRefund(000, "Refund");
        } catch (BankException e) {
            errormessage = e.getMessage();
        } catch (TransactionException transactionException) {
            errormessage = transactionException.getMessage();
        }
    }

    @And("the customers {int} is")
    public void theCustomersNewBalanceIs(int balance) throws BankException {
        Assert.assertEquals(BigDecimal.valueOf(balance), bankAdapter.getAccountBalance(customer.getAccountNumber()));
    }

    @And("the merchants {int} is")
    public void theMerchantsNewBalanceIs(int balance) throws BankException {
        Assert.assertEquals(BigDecimal.valueOf(balance), bankAdapter.getAccountBalance(merchant.getAccountNumber()));
    }

    @When("the customer makes the same refund tow times in a row")
    public void theCustomerMakesTheSameRefundTowTimesInARow() throws CustomerException, MerchantException {
        try {
            customerBalanceBeforeRefund = bankAdapter.getAccountBalance(customer.getAccountNumber());
            merchantBalanceBeforeRefund = bankAdapter.getAccountBalance(merchant.getAccountNumber());
            paymentGateway.doRefund(transaactionId, "Refund");
            errormessage = paymentGateway.doRefund(transaactionId, "Refund");
        } catch (BankException e) {
            errormessage = e.getMessage();
        } catch (TransactionException transactionException) {
            errormessage = transactionException.getMessage();
        }
    }
}
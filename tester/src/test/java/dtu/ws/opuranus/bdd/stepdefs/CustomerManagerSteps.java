package dtu.ws.opuranus.bdd.stepdefs;

import cucumber.api.java8.En;
import dtu.ws.opuranus.exceptions.BankException;
import dtu.ws.opuranus.interfaces.BankAdapterInterface;
import dtu.ws.opuranus.interfaces.CustomerManagerInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.exceptions.CustomerException;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

/**
 * @author Daniel
 */
public class CustomerManagerSteps implements En {

    @Autowired
    private CustomerManagerInterface customerManager;

    @Autowired
    BankAdapterInterface bankAdapter;

    String cpr;
    String errorMsg;
    Customer customer = new Customer();
    Iterable<Customer> customerIterator;
    Customer customer1;
    int counter = 0;

    @After
    public void cleanUpCustomManager() throws BankException, CustomerException {
        if(customer != null && customer.getAccountNumber() != null){
            bankAdapter.deleteAccount(customer.getAccountNumber());
            customerManager.deleteCustomer(customer);
        }
        if(customer1 != null ){
            bankAdapter.deleteAccount(customer1.getAccountNumber());
            customerManager.deleteCustomer(customer1);
        }

        if(customerIterator != null){
            for(Customer customer : customerIterator){
                customerManager.deleteCustomer(customer);
            }
        }

        counter = 0;
        cpr = "";
        errorMsg = "";
    }
    @Given("a user is created with the name {string}, and CPR number {string}")
    public void aUserIsCreatedWithTheNameAndCPRNumber(String arg0, String arg1) throws CustomerException, BankException {
        customer = customerManager.createCustomer(arg0, arg1);
        customer.setAccountNumber(bankAdapter.createAccount(customer, BigDecimal.valueOf(0)));
        customerManager.saveCustomer(customer);
        cpr = arg1;
    }

    @When("the user checks his account information")
    public void theUserChecksHisAccountInformation() throws CustomerException {
        customerManager.getCustomerByCpr(cpr);
    }

    @Then("the name stated is {string} and the CPR number is {string}")
    public void theNameStatedIsAndTheCPRNumberIs(String arg0, String arg1) throws CustomerException {
        Assert.assertEquals(customer.getName(), arg0);
        Assert.assertEquals(customer.getCpr(), arg1);
    }

    @Given("an existent account {string}, {string}")
    public void anExistentAccount(String arg0, String arg1) throws CustomerException, BankException {
        customer = customerManager.createCustomer(arg0, arg1);
        customer.setAccountNumber(bankAdapter.createAccount(customer, BigDecimal.valueOf(0)));
        customerManager.saveCustomer(customer);
        Assert.assertEquals(customer.getCpr(), arg1);
        Assert.assertEquals(customer.getName(), arg0);
    }

    @When("I retire that account")
    public void iRetireThatAccount(){
        try {
            customerManager.getCustomerByCpr("12345");
        } catch (CustomerException e) {
            errorMsg =  e.getMessage();
        }
    }

    @Then("I can create a new account with the same information {string}, {string}")
    public void iCanCreateANewAccountWithTheSameInformation(String arg0, String arg1) throws CustomerException {
        try {
            customer = customerManager.createCustomer(arg0, arg1);
            customer.setAccountNumber(bankAdapter.createAccount(customer, BigDecimal.valueOf(0)));
            customerManager.saveCustomer(customer);
            Assert.assertEquals(customer.getCpr(), customerManager.getCustomerByCpr(arg1).getCpr());
        } catch (CustomerException | BankException e) {
            errorMsg = e.getMessage();
        }
    }

    @And("I don't get an error message")
    public void iDonTGetAnErrorMessage() throws CustomerException {
        Assert.assertNotEquals(errorMsg, "");
    }

    @Given("that account Id {string} does not exist")
    public void thatAccountIdDoesNotExist(String arg0) throws CustomerException {
        try{
            customerManager.getCustomerByCpr(arg0);
        }catch (Exception e){
            errorMsg = e.getMessage();
        }
    }

    @Then("I get the error message {string}")
    public void iGetTheErrorMessage(String arg0) throws CustomerException {
        Assert.assertEquals(arg0, errorMsg);
    }




    @Given("a user with the name {string} and CPR number {string}")
    public void aUserWithTheNameAndCPRNumber(String arg0, String arg1) throws CustomerException, BankException {
       customer =  customerManager.createCustomer(arg0, arg1);
        customer.setAccountNumber(
                bankAdapter.createAccount(customer, BigDecimal.valueOf(0))
        );
        customerManager.saveCustomer(customer);
    }

    @When("getting the account information for the user with CPR number {string}")
    public void gettingTheAccountInformationForTheUserWithCPRNumber(String arg0) throws CustomerException {
        try{
            customerManager.getCustomerByCpr(arg0);
        }catch (Exception e){
            errorMsg = e.getMessage();
        }
    }

    @Then("The system returns an error message {string}")
    public void theSystemReturnsAnErrorMessage(String arg0) throws CustomerException {
        Assert.assertEquals(errorMsg, arg0);
    }
}


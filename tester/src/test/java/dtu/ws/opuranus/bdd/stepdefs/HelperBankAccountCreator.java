/**
 * @author Heino
 */


package dtu.ws.opuranus.bdd.stepdefs;

import dtu.ws.opuranus.interfaces.BankAdapterInterface;
import dtu.ws.opuranus.interfaces.CustomerManagerInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.exceptions.BankException;
import dtu.ws.opuranus.exceptions.CustomerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class HelperBankAccountCreator {
/*
    @Autowired
    BankAdapterInterface bankAdapter;

    @Autowired
    CustomerManagerInterface customerManager;

    public void createBankAccount(Customer customer) throws BankException, CustomerException {
        createBankAccount(customer, new BigDecimal(0));
    }

    public void createBankAccount(Customer customer, BigDecimal amount) throws BankException, CustomerException {
        String[] nameSplit = customer.getName().split(" ", 2);
        customer.setAccountNumber(bankAdapter.createAccount(nameSplit[0], nameSplit[1], customer.getCpr(), amount));
        customerManager.saveCustomer(customer);
    }

    public void closeBankAccount(Customer customer) throws BankException, CustomerException {
        bankAdapter.deleteAccount(customer.getAccountNumber());
        customer.setAccountNumber(null);
        customerManager.saveCustomer(customer);
    }


    public Customer createCustomerAndBankAccount(String name, String cpr, BigDecimal amount) throws BankException, CustomerException {
        Customer customer = customerManager.createCustomer(name, cpr);

        createBankAccount(customer, amount);

        return customer;
    }*/

}

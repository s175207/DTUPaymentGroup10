#@author Mathias
@Feature:Payment
@Cleanup:Users
Feature: Payment
  Description:

  @Heino
  Scenario: Payment
    Given the customer "Noe Bodyyxy" with cpr "123456-7892" is registered with DTU Pay
    And the customer has an account with the bank with a balance of 1000 kr
    And the customer has requested 1 token
    And the customer has at least 1 token left
    And the merchant "Ugly Harald" with cvr "12345678" is registered with DTU pay
    And the merchant has an account with the bank with a balance of 10000 kr
    And the merchant has scanned the customers token
    And the token is valid
    When the merchant requests 250 kr from the customer using the token
    Then the transaction is successful
    And the balance of the customer's bank account is 750 kr
    And the balance of the merchant's bank account is 10250 kr
    And the customer has 0 valid token

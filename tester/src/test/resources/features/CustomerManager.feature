#@author Daniel

Feature: Customer
  Description: Customer...

  '
  Scenario: Create an account and check the information match.
    Given a user is created with the name "Bentee Svende", and CPR number "999234965-2744"
    When the user checks his account information
    Then the name stated is "Bentee Svende" and the CPR number is "999234965-2744"

  Scenario: Create an account, delete it, and check the account no longer exists.
    Given an existent account "Toshaaa A. Petersen", "111130840-3897"
    When I retire that account
    Then I can create a new account with the same information "Toshaaa A. Petersen", "111130840-3897"
    And I don't get an error message

  Scenario: Retire a non existing account
    Given that account Id "12345" does not exist
    When I retire that account
    Then I get the error message "Customer does not exist"

  Scenario: Get non-existent account
    Given a user with the name "Andersaa Andersen" and CPR number "1213456-7890"
    When getting the account information for the user with CPR number "1213456-7891"
    Then The system returns an error message "Customer does not exist"


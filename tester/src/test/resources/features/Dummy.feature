# @author Mathias

Feature: Temporary test file to check that tests are run with Jenkins.
  This test file is only there to ensure that Cucumber tests are run on the server by Jenkins.
  This test should ALWAYS pass.

  Scenario: Test...

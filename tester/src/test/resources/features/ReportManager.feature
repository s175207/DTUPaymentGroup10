#@author Rasmus

## new feature
## Tags: optional
#
#Feature: UserReportScenarios
#  Description: The customer or the merchant can request a report digesting his DTUPay transactions for the last month.
#
#  Scenario: A user request a monthly digest of transactions for the last 30 days. The user has transactions.
##    Pre-requisite for this test is that test costumers and merchants are setup according to the scheme below.
#
#    Given I am a customer of DTUPay
#    And I have used DTUPay two times within the last month
#    When I request to get my monthly report
#    Then I should get a digest of both my transactions
#      | Date of sale     | Token ID | Customer ID | Merchant ID | Amount Paid | Refund Status |
#      | 2020-01-20 04:15 |  1       | 2           | 1           | 2.75        | -             |
#      | 2020-01-20 04:15 |  2       | 2           | 1           | 3.75        | -             |
#
#  Scenario: A user request a monthly digest of transactions for the last 30 days. No transactions.
##    Pre-requisite for this test is that test costumers and merchants are setup according to the scheme below.
#
#    Given I am a customer of DTUPay
#    And I have done no transactions using DTUPay within the last month
#    When I request to get my monthly report
#    Then I should get a "no transactions found" message
#
#
#
#
#  Scenario: A user (merchant) request a monthly digest of transactions for the last 30 days.
##   Pre-requisite for this test is that test costumers and merchants are setup according to the scheme below.
#
#    Given I am a merchant registered with DTUPay
#    And I have used DTUPay two times within the last month
#    When I request to get my monthly merchant report
#    Then I should get a digest of all transactions I've done in the last month
#      | Date of sale     | Token ID | Customer ID | Merchant ID | Amount Paid | Refund Status |
#      | 2020-01-20 04:15 |  1       | 0           | 7           | 2.75        | -             |
#      | 2020-01-20 04:15 |  2       | 0           | 7           | 3.75        | -             |
#
#
#  Scenario: A user (merchant) request a monthly digest of transactions for the last 30 days. He has had no transactions.
##   Pre-requisite for this test is that test costumers and merchants are setup according to the scheme below.
#
#    Given I am a merchant registered with DTUPay
#    And I have had no customers do transactions on DTUPay within the last month
#    When I request to get my monthly report
#    Then I should get a "no transactions found" message

#@author Mathias

Feature: Payment and Refund

  Scenario Outline: Payment
    Given the customer has an account with DTU pay with a balance of <customerBalance> kr.
    And the customer has <numOfTokens> tokens
    And a merchant has an account with DTU pay with a balance of <merchantBalance> kr.
    When the customer pay the merchant <payment> kr with the description <paymentDescription>.
    Then the system responds with <response>
    And the customers <customers new balance> is
    And the merchants <merchants new balance> is
    Examples: # the first succeed, the rest will fail
      | customerBalance | numOfTokens | merchantBalance | payment | customers new balance | merchants new balance|paymentDescription|response|
      | 500             | 1           | 500             | 100     | 400                   | 600                  |"Payment"         |""      |
      | 0               | 1           | 500             | 100     | 0                     | 500                  |"Payment"         |"Debtor balance will be negative"     |
      | 500             | 1           | 500             | -50     | 500                   | 500                  |"Payment"         |"Amount must be positive"  |
      | 500             | 1           | 500             | 100     | 500                   | 500                  |""                |"Description missing"  |

  Scenario: Refund succeed
    Given the customer has an account with DTU pay with a balance of 500 kr.
    And the merchant has an account with DTU pay with a balance of 500 kr.
    And the customer pay the merchant 100 kr and with the description "Payment".
    When the customer makes a refund on the purchase
    Then the system responds with "Refund succeed"
    And the money is transferred from the merchant to the customer

  Scenario: Refund failed, merchant balance is 0
    Given the customer has an account with DTU pay with a balance of 500 kr.
    And the merchant has an account with DTU pay with a balance of 0 kr.
    And the customer pay the merchant 100 kr and with the description "Payment".
    And the merchant spends some of the money
    When the customer makes a refund on the purchase
    Then the system responds with "Debtor balance will be negative"
    And the refund is rejected and no money i transferred

  Scenario: Refund failed, non existing purchase
    Given the customer has an account with DTU pay with a balance of 500 kr.
    And the merchant has an account with DTU pay with a balance of 500 kr.
    When the customer makes a refund on a non existing purchase
    Then the system responds with "Can refund non existing purchase"
    And the refund is rejected and no money i transferred

  Scenario: Refund failed, purchase already refund
    Given the customer has an account with DTU pay with a balance of 500 kr.
    And the merchant has an account with DTU pay with a balance of 500 kr.
    And the customer pay the merchant 100 kr and with the description "Payment".
    When the customer makes the same refund tow times in a row
    Then the system responds with "Transaction already refunded"
    And the refund is rejected and no money i transferred
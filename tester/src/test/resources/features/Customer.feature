#@author Daniel
@Feature:Customer
@Cleanup:Users
Feature: Customer
  Description: This test creates and deletes users.

  @Heino
  Scenario: Customer creation
    Given a customer's name is "Noe Body"
    And the customer's cpr is "123456-7890"
    And the customer does not exist
    When the administrator tries to create the customer
    Then the customer is successfully created

  @Heino
  Scenario: Customer creation, already existing user
    Given a customer's name is "Existing Customer"
    And the customer's cpr is "098765-4321"
    And the customer already exists
    When the administrator tries to create the customer
    Then creation of the customer fails

  @Heino
  Scenario: Customer deletion
    Given a customer's name is "Existing Customer"
    And the customer's cpr is "098765-4321"
    And the customer exists
    When the administrator tries to delete the customer
    Then the customer is successfully deleted

  @Heino
  Scenario: Customer deletion, non-existing user
    Given a customer's name is "Noe Body"
    And the customer's cpr is "123456-7890"
    And the customer does not exist
    When the administrator tries to delete the customer
    Then deletion of the customer fails

#@author Anders

Feature: The user requests some tokens
  The customer can request 1 to 5 tokens if either
  - He has spent all his or her tokens
  - It is the first time he or her requests
  - He only has one unused token left
  Overall, a customer can only have at most 6 unused tokens.

  Scenario Outline: The customer request between -1 and 6 tokens.
  I request some tokens, and based on how many tokens i already have,
  i get some more tokens i get the requested tokens or an error message.

    Given I have <Balance> tokens
    When I request to get <Request> new tokens
    Then I should get <Response>
    And I should have <Updated Balance> tokens

    Examples: Token Request went well
      | Balance | Request | Updated Balance | Response |
      | 0       | 1       | 1               | ""       |
      | 1       | 5       | 6               | ""       |


    Examples: Token request failed
      | Balance | Request | Updated Balance | Response                                  |
      | 0       | -1      | 0               | "You can request between 1 and 5 tokens"  |
      | 1       | 6       | 1               | "You can request between 1 and 5 tokens"  |
      | 2       | 1       | 2               | "You can't request tokens when you have 2 or more tokens"  |

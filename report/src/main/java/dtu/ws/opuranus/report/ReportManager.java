package dtu.ws.opuranus.report;

import dtu.ws.opuranus.entity.Token;
import dtu.ws.opuranus.interfaces.ReportManagerInterface;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Transaction;
import dtu.ws.opuranus.exceptions.TransactionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
/**
 * @author Rasmus
 */

@Service
@Primary
public class ReportManager implements ReportManagerInterface {

    @Autowired
    private TransactionRepository transactionRepository;

    private ArrayList<Transaction> transactionList = new ArrayList<Transaction>();

    /**
     * Gets a string containing the data from the customers transactions for the last 30 days.
     * @param customer
     * @return
     */
    public String transactionReport(Customer customer) throws TransactionException {
        List<Transaction> transactions = generateReport(customer.getId());
        String initial = "Date of sale, Token ID, Customer ID, Merchant ID, Amount Paid, Refund Status\n";
        String s =initial;
        for(Transaction t : transactions) {
            Date thirty = new Date(System.currentTimeMillis() - (30l * 24l * 60l * 60l * 1000l));
            if (t.getTransactionTime().after(thirty)) {
                s = s + t.toString() + "\n";
            }
        }
        if (s.equals(initial))
            s= "no transactions found";
        return s;
    }

    /**
     * Gets a string containing the data from the merchants transactions for the last 30 days.
     * @param merchant
     * @return
     */
    public String transactionReport(Merchant merchant) throws TransactionException {
        List<Transaction> transactions = generateReport(merchant);
        String initial = "Date of sale, Token ID, Customer ID, Merchant ID, Amount Paid, Refund Status\n";
        String s =initial;
        for(Transaction t : transactions) {
            t.setCustomerId(0);
            Date thirty = new Date(System.currentTimeMillis() - (30l * 24l * 60l * 60l * 1000l));
            if (t.getTransactionTime().after(thirty)) {
                s = s + t.toString() + "\n";
            }
        }
        if (s.equals(initial))
            s= "no transactions found";
        return s;
    }

    /**
     * Returns a list of transactions from a given user
     * @param customerID
     * @return
     * @throws TransactionException
     */
    private List<Transaction> generateReport(Integer customerID) throws TransactionException {

            if (transactionRepository == null) {
                transactionList = null;
                return transactionList;
            }
            List<Transaction> transactions = transactionRepository.findByCustomerIdAndTransactionTimeGreaterThanEqualOrderByTransactionTime(
                    customerID,
                    new Date(30l * 24l * 60l * 60l * 1000l)
            );
            return transactions;
        }

        /**
         *      * Returns a list of transactions from a given merchant
         * @param merchant
         * @return
         * @throws TransactionException
         */
        private List<Transaction> generateReport (Merchant merchant) throws TransactionException {

//      Retrieve Transactions from customer ID
            List<Transaction> transactions = transactionRepository.findByMerchantIdAndTransactionTimeGreaterThanEqualOrderByTransactionTime(
                    merchant.getId(),
                    new Date(30l * 24l * 60l * 60l * 1000l)
            );
            return transactions;
        }

        /**
         * Log a new transaction in the database.
         * @param customer
         * @param merchant
         * @param token
         * @param amount
         * @return
         */
        public Transaction logNewTransaction (Customer customer, Merchant merchant, Token token, BigDecimal amount){
            Transaction transaction = new Transaction(customer, merchant, token, amount);
            transactionRepository.save(transaction);
            System.out.println("transaction_id: " + transaction.getSaleId());
            return transaction;
        }

        /**
         * get a transaction from a transaction id
         * @param transactionId
         * @return
         * @throws TransactionException
         */
        public Transaction getTransactionById ( int transactionId) throws TransactionException {
            Optional<Transaction> transaction = transactionRepository.findById(transactionId);

            if (!transaction.isPresent()) {
                throw new TransactionException("Can refund non existing purchase");
            }

            return transaction.get();
        }

        /**
         * Mark a transaction as refunded.
         * @param transaction
         * @return
         * @throws TransactionException
         */
        public Transaction markRefunded (Transaction transaction) throws TransactionException {
            transaction.setRefunded(new Date());
            transactionRepository.save(transaction);
            return transaction;
        }

        /**
         *
         * @param transaction
         * @param date
         */
        public void setTime (Transaction transaction, Date date){
            transaction.setTransactionTime(date);
            transactionRepository.save(transaction);
        }

        /**
         * Deletes all Transactions.
         */
        public void deleteAll () {
            transactionRepository.deleteAll();

        }

    }


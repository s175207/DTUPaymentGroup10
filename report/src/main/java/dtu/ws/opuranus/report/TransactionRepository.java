package dtu.ws.opuranus.report;

import dtu.ws.opuranus.entity.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author Rasmus
 * Transactions are the DTUPay standard object for managing all user transactions and token expenditure.
 * TransactionRepository is the class used by DTUPay to administrate Transactions in the database.
 * TransactionRepository extends CrudRepository giving the class Create, Read, Update, Delete functionalities.
 */
@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

    /**
     * @param id
     * @return
     */
    @Override
    Optional<Transaction> findById(@Param("id") Integer id);

    /**
     * @param customerId
     * @return
     */
    Set<Transaction> findByCustomerId(@Param("customerId") Integer customerId);

    /**
     * @param merchantID
     * @return
     */
    Set<Transaction> findByMerchantId(@Param("merchantID") Integer merchantID);

    /**
     * @param customerId
     * @param date
     * @return
     */
    List<Transaction> findByCustomerIdAndTransactionTimeGreaterThanEqualOrderByTransactionTime(
            @Param("customerId") Integer customerId,
            @Param("date") Date date
    );

    /**
     * @param customerId
     * @param from
     * @param to
     * @return
     */
    List<Transaction> findByCustomerIdAndTransactionTimeBetweenOrderByTransactionTime(
            @Param("customerId") Integer customerId,
            @Param("from") Date from,
            @Param("to") Date to
    );

    /**
     * @param customerId
     * @param date
     * @return
     */
    List<Transaction> findByMerchantIdAndTransactionTimeGreaterThanEqualOrderByTransactionTime(
            @Param("merchantId") Integer customerId,
            @Param("date") Date date
    );

    /**
     * @param customerId
     * @param from
     * @param to
     * @return
     */
    List<Transaction> findByMerchantIdAndTransactionTimeBetweenOrderByTransactionTime(
            @Param("customerId") Integer customerId,
            @Param("from") Date from,
            @Param("to") Date to
    );

}

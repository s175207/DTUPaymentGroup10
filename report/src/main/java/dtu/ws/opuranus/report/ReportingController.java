
package dtu.ws.opuranus.report;

import dtu.ws.opuranus.TransactionTimeTransferObject;
import dtu.ws.opuranus.TransactionTransferObject;
import dtu.ws.opuranus.entity.Customer;
import dtu.ws.opuranus.entity.Merchant;
import dtu.ws.opuranus.entity.Transaction;
import dtu.ws.opuranus.exceptions.TokenException;
import dtu.ws.opuranus.exceptions.TransactionException;
import dtu.ws.opuranus.interfaces.ReportManagerInterface;
import dtu.ws.opuranus.exceptions.TransactionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Rasmus
 */


//@JsonIgnoreProperties
@RestController
@RequestMapping("report")
public class ReportingController {

    @Autowired
    ReportManagerInterface reportManager;

    @GetMapping("/transactionReportCustomer")
    public ResponseEntity transactionReport(
            @Param("customer") Customer customer
    ) {
        try {
            String s = reportManager.transactionReport(customer);
            return ResponseEntity
                    .ok(s);

        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }

    @GetMapping("/transactionReportMerchant")
    public ResponseEntity transactionReport(
            @Param("merchant") Merchant merchant
    ) throws TransactionException {
        String s = reportManager.transactionReport(merchant);
        return ResponseEntity
                .ok(s);
    }

    @PostMapping("/logNewTransaction")
    public ResponseEntity logNewTransaction(
            @RequestBody TransactionTransferObject transfer
    )  {
        Transaction t = reportManager.logNewTransaction(transfer.getCustomer(),
                transfer.getMerchant(), transfer.getToken(), transfer.getAmount());

        return ResponseEntity.ok(t);
    }

    @GetMapping("/getTransactionById")
    public ResponseEntity getTransactionById(
            @Param("transactionId") int transactionId
    ) {
        Transaction t = null;
        try{
            t = reportManager.getTransactionById(transactionId);
            return ResponseEntity.ok(t);
        } catch (TransactionException transactionException) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(transactionException.getMessage());
        }
    }



    @PutMapping("/markRefunded")
    public ResponseEntity markRefunded(
            @RequestBody Transaction transaction

    ) {
        try {
            reportManager.markRefunded(transaction);
            return ResponseEntity.noContent().build();

        } catch (TransactionException transactionException) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(transactionException.getMessage());
        }
    }

    @PutMapping("/setTime")
    public ResponseEntity setTime(
            @RequestBody TransactionTimeTransferObject transfer
    ) {
        reportManager.setTime(transfer.getTransaction(),transfer.getDate());
        return ResponseEntity.noContent().build();
    }


    @DeleteMapping("/delete")
    public ResponseEntity deleteAll(){
        reportManager.deleteAll();
        return ResponseEntity.noContent().build();
    }
}

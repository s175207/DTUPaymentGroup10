package dtu.ws.opuranus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rasmus
 */
@SpringBootApplication(
        scanBasePackages = {
                "dtu.ws.opuranus"
        }
)
public class ReportApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReportApplication.class, args);

    }
}
